/*global require, describe, it, browser,before, after, beforeEach, exist*/

var Page = require('../../../../../pageobjects/page');
var expect = require('chai').expect;
var CreateAccountPage = require('../../../../../pageobjects/create_account.page');
var ShippingDetailsPage = require('../../../../../pageobjects/shipping_details.page');
var BusinessDetailPage = require('../../../../../pageobjects/business_details.page');
var PersonalDetailPage = require('../../../../../pageobjects/personal_details.page');
var BankDetailPage = require('../../../../../pageobjects/bank_details.page');
var AccountActivationPage = require('../../../../../pageobjects/account_activation.page');
var DBValidation = require('../../../../../pageobjects/db_validation');
var ShopPage = require('../../../../../pageobjects/shop.page');
var DBconfig = require('../../../dbconfig/dbconfig_staging');
var ReadFile = require('fs');
var contents = ReadFile.readFileSync('test/specs/data/test_account.json');

var localeList = [{"locale" : "en-be"}];
var testData = JSON.parse(contents);
var prcCode = 'prc=VBANCONTACT1';

('use strict');
describe('Account creation and activation', function() {

  function createAccount(localeObject) {
  it('and user should able to order a terminal using bankwire as sole traders for '+localeObject.locale, async function() {
    var country = localeObject.locale.split('-')[1];
    console.log('country' + country);
      browser.url('/' + `${localeObject.locale}` + '/signup/create-account?'+ prcCode);
      browser.windowHandleSize({ width: 1300, height: 768 });
      var account = CreateAccountPage.getAccount(country);
      console.log('account' + account);
      CreateAccountPage.fillEmailAndPassword(account, account);
      CreateAccountPage.continue.click();
      ShippingDetailsPage.fillShippingDetails(country, testData);
      //ShippingDetailsPage.applyVoucher();
      ShippingDetailsPage.selectAndFillCreditCardDetails();
      ShippingDetailsPage.clickConfirmOrder();
      ShippingDetailsPage.goToShop(localeObject.locale,prcCode);
      ShopPage.enterShippingDetailsAndPayUsingCC(testData,country);
      await DBValidation.voucherValidationForBE(country, account);
  });}

  for (var i = 0; i < localeList.length; i++) {
    var localeObject = localeList[i];
    console.log('localeObject' + localeObject);
    createAccount(localeObject);
  }
});
