const Page = require('./page');
const ReadFile = require('fs');

const contents = ReadFile.readFileSync('test/specs/data/user_details.json');
const userData = JSON.parse(contents);
const cnpjValue = ReadFile.readFileSync('test/specs/data/cnpj.json');
const cnpjData = JSON.parse(cnpjValue);
const cpfValue = ReadFile.readFileSync('test/specs/data/cpf.json');
const cpfData = JSON.parse(cpfValue);
const orderTerminalButton = '/html/body/dialogue/div[1]/div/div[2]/ul/li/a';
const firstNameText = '#shipping_first_name';
const lastNameText = '#shipping_last_name';
const address1Text = '#shipping_address_line1';
const cityText = '#shipping_city';
const zipcodeText = '#shipping_post_code';
const regionForCLDropDown = '#shipping_parent_region_id';
const cardholderNameText = 'input[name="cardholderName"]';
const cardNumberText = '#cardNumber';
const expirationDateDropDown = 'select[name="month"]';
const yearDropDown = 'select[name="year"]';
const verificationCodeText = 'input[name="cvv"]';
const confirmOrderButton = '.btn.btn--highlight._8a35ff3f1f1a9f6e889f06d44e3fe25f';
const ccPaymentMethodOption = 'label[for="payment-method-credit_card"]';
const sofortOption = 'label[for="payment-method-sofortbanking"]';
const iDEALOption = 'label[for="payment-method-ideal"]';
const houseNumberText = '#shipping_house_number';
const regionText = '#shipping_region_id';
const voucherPlus = '/html/body/div[2]/ui-view/layout/div[1]/div/div/new-shop/div/div/section/div[2]/div/div[1]/div[2]/div/div[1]/li';
const voucherTxtt = '[name="voucherCode"]';
const voucherButton = '.btn.btn--grey';
const bankwireOption = 'label[for="payment-method-bankwire"]';
const skipPurchase = '._10fe2a74ada1ebe92660b8584acd31b0';
const confirmOnSkipPurchaseModelButton = 'button.btn.btn--secondary';
const soleTraders = 'label[for="radio-option-SOLE_TRADER"]';
const company = 'label[for="radio-option-BUSINESS"]';
const completeAccountBtt = 'button.btn.btn--highlight';
const alreadyHaveTerminalLink = 'a[ng-click="dialogue.cancelOrderAndClose()"]';
const postalCodeText = '#postCode';
const CPFText = '#SOLE_TRADER';
const CNPJTxt = '#BUSINESS';
const boletoOption = 'label[for="payment-method-boleto"]';
const shippingCountry ='#shipping_country';
const vatIDText = 'input.input.ng-valid.input--optional';
const vatRate = '/html/body/div[2]/ui-view/ui-view/signup-shop/div/div/div/div[2]/section/div[2]/div/div[2]/div/div[2]/div/div[1]/ul[2]/li[1]/span[1]';
const vatIDlabl = '.label.label--optional';
const billingCheckbox = '[for="useDifferent"]';
const billingFirstNameText = '#billing_first_name';
const billingLastNameText = '#billing_last_name';
const billingAddressLine1Text = '#billing_address_line1';
const billingPostCodeText = '#billing_post_code';
const billingCityText = '#billing_city';
const billingCountryDropDown = '#billing_country';
const addOrderToCartForTopButton = '.btn.btn--highlight._6bcd4b2281f5678313f55cbf20c36cc9._4aec7908310ea49cebf2d9ae819f818c';
const addOrderToCartForSuperButton = '//*[@id="root"]/ui-view/layout/div[1]/div/div/new-shop/div/div/div/div/div/div/div[2]/button/div/span';
const ShippingMobilePhoneText = '#shipping_mobile_phone';
const phonePlus = '//*[@id="root"]/ui-view/ui-view/signup-shop/div/div/div/div[2]/section/div[3]/div/div[1]/div[1]/div/div/form[1]/ul[2]/li[2]';
const phoneText = '#shipping_landline';

const ShippingDetailsPage = Object.create(Page, {
  orderCardTerminal: {
      get: function () {
          return browser.element(orderTerminalButton);
      }
  },

  ShippingMobilePhone: {
      get: function () {
          return browser.element(ShippingMobilePhoneText);
      }
  },

  addOrderToCartForTop: {
      get: function () {
          return browser.element(addOrderToCartForTopButton);
      }
  },

  addOrderToCartForSuper: {
      get: function () {
          return browser.element(addOrderToCartForTopButton);
      }
  },

  alreadyHaveTerminal: {
      get: function () {
          return browser.element(alreadyHaveTerminalLink);
      }
  },

  billingAddressCheckbox: {
      get: function () {
          return browser.element(billingCheckbox);
      }
  },

  billingFirstName: {
      get: function () {
          return browser.element(billingFirstNameText);
      }
  },

  billingLastName: {
      get: function () {
          return browser.element(billingLastNameText);
      }
  },

  billingAddressLine1: {
      get: function () {
          return browser.element(billingAddressLine1Text);
      }
  },

  billingPostCode: {
      get: function () {
          return browser.element(billingPostCodeText);
      }
  },

  billingCity: {
      get: function () {
          return browser.element(billingCityText);
      }
  },

  billingCountry: {
      get: function () {
          return browser.element(billingCountryDropDown);
      }
  },

  firstNameFeild: {
      get: function () {
          return browser.element(firstNameText);
      }
  },

  phoneFeild: {
      get: function () {
          return browser.element(phoneText);
      }
  },

  phonePlusFeild: {
      get: function () {
          return browser.element(phonePlus);
      }
  },

  vatIDlabel: {
      get: function () {
          return browser.element(vatIDlabl);
      }
  },

  lastNameFeild: {
      get: function () {
          return browser.element(lastNameText);
      }
  },

  address1: {
      get: function () {
          return browser.element(address1Text);
      }
  },

  city: {
      get: function () {
          return browser.element(cityText);
      }
  },

  shippingToCountry: {
      get: function () {
          return browser.element(shippingCountry);
      }
  },

  zipcode: {
      get: function () {
          return browser.element(zipcodeText);
      }
  },

  enterVatID: {
      get: function () {
          return browser.element(vatIDText);
      }
  },

  ccPaymentMethod: {
      get: function () {
          return browser.element(ccPaymentMethodOption);
      }
  },

  sofort: {
      get: function () {
          return browser.element(sofortOption);
      }
  },

  iDEAL: {
      get: function () {
          return browser.element(iDEALOption);
      }
  },

  cardName: {
      get: function () {
          return browser.element(cardholderNameText);
      }
  },

  cardNumber: {
      get: function () {
          return browser.element(cardNumberText);
      }
  },

  regionForCL: {
      get: function () {
          return browser.element(regionForCLDropDown);
      }
  },

  expirationDate: {
      get: function () {
          return browser.element(expirationDateDropDown);
      }
  },

  year: {
      get: function () {
          return browser.element(yearDropDown);
      }
  },

  verificationCode: {
      get: function () {
          return browser.element(verificationCodeText);
      }
  },

  confirmOrder: {
      get: function () {
          return browser.element(confirmOrderButton);
      }
  },

  houseNumber: {
      get: function () {
          return browser.element(houseNumberText);
      }
  },

  region: {
      get: function () {
          return browser.element(regionText);
      }
  },

  bankWire: {
      get: function () {
          return browser.element(bankwireOption);
      }
  },

  skipPurchaseLink: {
      get: function () {
          return browser.element(skipPurchase);
      }
  },

  voucher: {
      get: function () {
          return browser.element(voucherPlus);
      }
  },

  voucherTxt: {
      get: function () {
          return browser.element(voucherTxtt);
      }
  },

  voucherBt: {
      get: function () {
          return browser.element(voucherButton);
      }
  },

  confirmOnSkipPurchaseModel: {
      get: function () {
          return browser.element(confirmOnSkipPurchaseModelButton);
      }
  },

  completeAccountBt: {
      get: function () {
          return browser.element(completeAccountBtt);
      }
  },

  CPF: {
      get: function () {
          return browser.element(soleTraders);
      }
  },

  CNPJ: {
      get: function () {
          return browser.element(company);
      }
  },

  CPFTxt: {
      get: function () {
          return browser.element(CPFText);
      }
  },

  CNPJTxt: {
      get: function () {
          return browser.element(CNPJTxt);
      }
  },

  CEP: {
      get: function () {
          return browser.element(postalCodeText);
      }
  },

  boleto: {
      get: function () {
          return browser.element(boletoOption);
      }
  },

  orderTerminal: {
      value: function () {
          browser.pause(2000);
          browser.waitForVisible(orderTerminalButton);
          ShippingDetailsPage.orderCardTerminal.click();
      }
  },

  goToShop: {
    value: function(locale,prcCode) {
      browser.pause(5000);
      browser.url('/' + `${locale}` + '/shop?product=air&'+prcCode);
    }
  },

  fillShippingDetails: {
      value: function (country,testData) {
          browser.waitForVisible(firstNameText);
          ShippingDetailsPage.firstNameFeild.setValue(userData["userDetails"]["firstName"]);
          ShippingDetailsPage.lastNameFeild.setValue(userData["userDetails"]["lastName"]);
          if(country=='no'){
            ShippingDetailsPage.ShippingMobilePhone.setValue(userData["userDetails"]["ShippingMobilePhone"]);
          }
          if(country!='br'){
          ShippingDetailsPage.address1.setValue(userData["userDetails"]["address"]);
          ShippingDetailsPage.city.setValue(userData["userDetails"]["city"]);
          }
          if(country=='br'){
            ShippingDetailsPage.CEP.setValue(testData[country]["postalCode"]);
          } else {
            ShippingDetailsPage.zipcode.setValue(testData[country]["postalCode"]);
          //  ShippingDetailsPage.shippingToCountry.selectByValue(testData[country]["shippingCountry"]);
          }
          switch(country){
            case 'br':
            case 'nl':
              browser.waitForVisible(houseNumberText);
              ShippingDetailsPage.houseNumber.setValue(userData["userDetails"]["houseNo"]);
              break;
          }
          switch(country){
            case 'it':
            case 'es':
            case 'us':
            case 'ie':
            case 'pt':
            case 'pl':
            ShippingDetailsPage.region.selectByIndex(3);
            break;
            case 'cl':
            ShippingDetailsPage.regionForCL.selectByIndex(3);
            ShippingDetailsPage.region.selectByIndex(3);
            break;
          }
      }
  },

  fillShippingDetailsForPhone: {
      value: function (country,testData,phone) {
          browser.waitForVisible(firstNameText);
          ShippingDetailsPage.firstNameFeild.setValue(userData["userDetails"]["firstName"]);
          ShippingDetailsPage.lastNameFeild.setValue(userData["userDetails"]["lastName"]);
          ShippingDetailsPage.phonePlusFeild.click();
          ShippingDetailsPage.phoneFeild.setValue(phone);
          if(country=='no'){
            ShippingDetailsPage.ShippingMobilePhone.setValue(userData["userDetails"]["ShippingMobilePhone"]);
          }
          if(country!='br'){
          ShippingDetailsPage.address1.setValue(userData["userDetails"]["address"]);
          ShippingDetailsPage.city.setValue(userData["userDetails"]["city"]);
          }
          if(country=='br'){
            ShippingDetailsPage.CEP.setValue(testData[country]["postalCode"]);
          } else {
            ShippingDetailsPage.zipcode.setValue(testData[country]["postalCode"]);
          //  ShippingDetailsPage.shippingToCountry.selectByValue(testData[country]["shippingCountry"]);
          }
          switch(country){
            case 'br':
            case 'nl':
              browser.waitForVisible(houseNumberText);
              ShippingDetailsPage.houseNumber.setValue(userData["userDetails"]["houseNo"]);
              break;
          }
          switch(country){
            case 'it':
            case 'es':
            case 'us':
            case 'ie':
            case 'pt':
            case 'pl':
            ShippingDetailsPage.region.selectByIndex(3);
            break;
            case 'cl':
            ShippingDetailsPage.regionForCL.selectByIndex(3);
            ShippingDetailsPage.region.selectByIndex(3);
            break;
          }
      }
  },

  fillShippingDetailsForVAT: {
      //value: function (country,shippingData,vatCountry) {
        value: function (country,shippingData,vatCountry) {
          browser.waitForVisible(firstNameText);
          ShippingDetailsPage.firstNameFeild.setValue(userData["userDetails"]["firstName"]);
          ShippingDetailsPage.lastNameFeild.setValue(userData["userDetails"]["lastName"]);
          if(country!='br'){
          ShippingDetailsPage.address1.setValue(userData["userDetails"]["address"]);
          ShippingDetailsPage.city.setValue(userData["userDetails"]["city"]);
          console.log('country '+country);
            console.log('test'+shippingData[country]["postalCode"]);
            ShippingDetailsPage.zipcode.setValue(shippingData[country]["postalCode"]);
            ShippingDetailsPage.shippingToCountry.selectByValue(shippingData[country]["shippingCountry"]);
            //console.log('vat rate '+browser.getText(vatRate));
            var vatPercentagebeforePharse = browser.getText(vatRate);
            var vatPercentage = vatPercentagebeforePharse.split(' ')[1];
            //console.log('vat rate '+vatPercentage);
          //  console.log('vat '+shippingData[vatCountry]["vatID"]);
            ShippingDetailsPage.enterVatID.setValue(shippingData[vatCountry]["vatID"]);
            ShippingDetailsPage.vatIDlabel.click();
            browser.pause(3000);
            //console.log('vat rate after apply vat'+browser.getText(vatRate));
            var vatPercentagebeforePharse1 = browser.getText(vatRate);
            var vatPercentage1 = vatPercentagebeforePharse1.split(' ')[1];
            //console.log('vat rate after apply vat'+vatPercentage1);
          }
          switch(country){
            case 'nl':
              browser.waitForVisible(houseNumberText);
              ShippingDetailsPage.houseNumber.setValue(userData["userDetails"]["houseNo"]);
              break;
          }
          switch(country){
            case 'it':
            case 'es':
            case 'us':
            case 'ie':
            ShippingDetailsPage.region.selectByIndex(3);
            break;
            case 'cl':
            ShippingDetailsPage.regionForCL.selectByIndex(3);
            ShippingDetailsPage.region.selectByIndex(3);
            break;
          }
      }
  },


  fillBillingAddress: {
      value: function (country) {
          browser.waitForVisible(billingCheckbox);
          ShippingDetailsPage.billingAddressCheckbox.click();
          ShippingDetailsPage.billingFirstName.setValue("test");
          ShippingDetailsPage.billingLastName.setValue("test");
          ShippingDetailsPage.billingAddressLine1.setValue("test");
          ShippingDetailsPage.billingPostCode.setValue(shippingData[country]["postalCode"]);
          ShippingDetailsPage.billingCity.setValue("test");
          ShippingDetailsPage.billingCountry.selectByValue(shippingData[country]["shippingCountry"]);
      }
  },

  selectAndFillCreditCardDetails: {
      value: function () {
          browser.waitForVisible(ccPaymentMethodOption);
          ShippingDetailsPage.ccPaymentMethod.scroll();
          ShippingDetailsPage.ccPaymentMethod.click();
          browser.waitForVisible(cardholderNameText);
          ShippingDetailsPage.cardName.setValue(userData["userDetails"]["cardHoldrName"]);
          ShippingDetailsPage.cardNumber.setValue(userData["userDetails"]["ccNumber"]);
          ShippingDetailsPage.expirationDate.selectByIndex(1);
          ShippingDetailsPage.year.selectByIndex(3);
          ShippingDetailsPage.verificationCode.setValue(userData["userDetails"]["code"]);
      }
  },

  selectAndFillBankWire: {
      value: function () {
          browser.waitForVisible(bankwireOption);
          ShippingDetailsPage.bankWire.scroll();
          ShippingDetailsPage.bankWire.click();
      }
  },

  selectSoleTraders: {
      value: function () {
          browser.waitForVisible(soleTraders);
          ShippingDetailsPage.CPF.click();
          ShippingDetailsPage.CPFTxt.setValue(cpfData["cpf"]);
      }
  },

  selectCompany: {
      value: function () {
          browser.waitForVisible(company);
          ShippingDetailsPage.CNPJ.click();
          ShippingDetailsPage.CNPJTxt.setValue(cnpjData["cnpj"]);
      }
  },

  applyVoucher: {
      value: function () {
          browser.waitForVisible(voucherPlus);
          ShippingDetailsPage.voucher.click();
          ShippingDetailsPage.voucherTxt.setValue(userData["userDetails"]["voucherCode"]);
          ShippingDetailsPage.voucherBt.click();
      }
  },

  selectSofort: {
      value: function () {
          browser.pause(1000);
          ShippingDetailsPage.sofort.scroll();
          ShippingDetailsPage.sofort.click();
      }
  },

  selectBoleto: {
      value: function () {
          browser.waitForVisible(boletoOption);
          ShippingDetailsPage.boleto.click();
      }
  },

  selectiDEAL: {
      value: function () {
          ShippingDetailsPage.iDEAL.scroll();
          ShippingDetailsPage.iDEAL.click();
      }
  },

  skipPurchase: {
      value: function () {
          browser.waitForVisible(skipPurchase);
          ShippingDetailsPage.skipPurchaseLink.click();
          ShippingDetailsPage.confirmOnSkipPurchaseModel.click();
          browser.pause(2000);
          browser.waitForVisible(orderTerminalButton);
          ShippingDetailsPage.orderCardTerminal.click();
      }
  },

  completeAccount: {
      value: function () {
          browser.pause(4000);
          browser.waitForVisible(completeAccountBtt);
          ShippingDetailsPage.completeAccountBt.click();
          browser.pause(2000);
      }
  },

  clickConfirmOrder: {
      value: function () {
        browser.pause(3000);
        browser.waitForVisible(confirmOrderButton);
        ShippingDetailsPage.confirmOrder.click();
        browser.pause(3000);
      }
  }

});

module.exports = ShippingDetailsPage;
