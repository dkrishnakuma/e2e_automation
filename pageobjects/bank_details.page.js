const Page = require('./page');

const actNumber = '#accountNumber';
//const actIban = '[name="iban"]';
const actIban = '#accountIban';
const continueButton = 'button.btn.btn--highlight.ng-binding';
//const continueButton = '.css-yvbbfo-ChildrenWrapper-childrenWrapperStyles.e1apts1r1';
const okButton = 'a.btn.btn--highlight.ng-scope';
const actNumberForSE = '#account_number';
const sortCode = '#account-sort-code';
//const swift = '[name="swift"]';
const swift = '[name="accountSwift"]';
const bankCodeTxt = '#bankCode';
const bankCodeTxtt = '#bank_code';
const branchCodeForBRText = '#branch_code';
const paymentTypeforBROption = '[for="payout_instrument_bank_account"]';
const continueForBRButton =
  'brazilian-bank-account-container button.btn.btn--highlight';
const accountNumberForBRText = '#account_number';
const checkDigitForBRText = '#check_digit';
const accountSubtypeForBRText = '#account_subtype';
const bankAccountDropDown = 'select[ng-model="$ctrl.bankAccount.bank_code"]';
const accountNumberText = '#account_number';
const accounTypeForBRText = '#account_type';
const continueBt = 'button[ng-disabled="brazilianBankAccountForm.$invalid || $ctrl.loading"]';

var h = new Object(); // or just {}
h['fi'] = 1;h['fr'] = 2;h['it'] = 3;h['es'] = 4;h['de'] = 5;h['nl'] = 6;
h['pl'] = 7;h['be'] = 8;h['ie'] = 9;h['at'] = 10;h['bg'] = 11;h['cy'] = 12;
h['cz'] = 13;h['hu'] = 14;h['lv'] = 15;h['lt'] = 16;h['si'] = 17;
h['ee'] = 18;h['lu'] = 19;h['mt'] = 20;h['sk'] = 21;h['pt'] = 22;

const BankDetailPage = Object.create(Page, {
  accountNumber: {
    get: function() {
      return browser.element(actNumber);
    }
  },

  bankAccountForCL: {
    get: function() {
      return browser.element(bankAccountDropDown);
    }
  },

  continueBtForBR: {
    get: function() {
      return browser.element(continueBt);
    }
  },

  accountNumberForCL: {
    get: function() {
      return browser.element(accountNumberText);
    }
  },

  accountSortCode: {
    get: function() {
      return browser.element(sortCode);
    }
  },

  saveAndContinue: {
    get: function() {
      return browser.element(continueButton);
    }
  },

  okBut: {
    get: function() {
      return browser.element(okButton);
    }
  },

  iban: {
    get: function() {
      return browser.element(actIban);
    }
  },

  bic: {
    get: function() {
      return browser.element(swift);
    }
  },

  bankCode: {
    get: function() {
      return browser.element(bankCodeTxt);
    }
  },

  bankCodeForSE: {
    get: function() {
      return browser.element(bankCodeTxtt);
    }
  },

  accountNumberForSE: {
    get: function() {
      return browser.element(actNumberForSE);
    }
  },

  paymentTypeforBR: {
    get: function() {
      return browser.element(paymentTypeforBROption);
    }
  },

  bankCodeForBR: {
    get: function() {
      return browser.element(bankCodeTxtt);
    }
  },

  branchCodeForBR: {
    get: function() {
      return browser.element(branchCodeForBRText);
    }
  },

  continueForBR: {
    get: function() {
      return browser.element(continueForBRButton);
    }
  },

  accounTypeForBR: {
    get: function() {
      return browser.element(accounTypeForBRText);
    }
  },

  accountNumberForBR: {
    get: function() {
      return browser.element(accountNumberForBRText);
    }
  },

  checkDigitForBR: {
    get: function() {
      return browser.element(checkDigitForBRText);
    }
  },

  accountSubtypeForBR: {
    get: function() {
      return browser.element(accountSubtypeForBRText);
    }
  },

  fillBankDetailsForBRCompany: {
    value: function() {
      browser.waitForVisible(bankCodeTxtt);
      BankDetailPage.bankCodeForSE.selectByIndex(1);
      BankDetailPage.branchCodeForBR.setValue('1234');
      BankDetailPage.accounTypeForBR.selectByIndex(2);
      BankDetailPage.accountSubtypeForBR.selectByIndex(2);
      BankDetailPage.accountNumberForBR.setValue('123456');
      BankDetailPage.checkDigitForBR.setValue('7');
      browser.pause(3000);
      browser.waitForVisible(continueBt);
      BankDetailPage.continueBtForBR.click();
      browser.pause(2000);
    }
  },

  fillBankDetails: {
    value: function(country, testData) {
      switch (country) {
        case 'ch':
          browser.waitForVisible(actIban);
          BankDetailPage.iban.setValue(testData[country]['IBAN']);
          break;
        case 'no':
          browser.waitForVisible(actNumber);
          BankDetailPage.accountNumber.setValue(testData[country]['accountNumber']);
          break;
        case 'gb':
        case 'dk':
            browser.waitForVisible(sortCode);
            BankDetailPage.accountNumber.setValue(testData[country]['accountNumber']);
            BankDetailPage.accountSortCode.setValue(testData[country]['sortCode']);
            break;
        case 'us':
          browser.waitForVisible(actNumber);
          BankDetailPage.accountNumber.setValue(testData[country]['accNumber']);
          BankDetailPage.bankCode.setValue(testData[country]['bankCode']);
          break;
        case 'cl':
          browser.waitForVisible(bankAccountDropDown);
          browser.pause(2000);
          BankDetailPage.bankAccountForCL.selectByValue('string:012');
          BankDetailPage.accountNumberForCL.setValue('123456');
          break;
        case 'se':
          browser.waitForVisible(actNumberForSE);
          BankDetailPage.accountNumberForSE.setValue(
            testData[country]['accountNumber']
          );
          browser.pause(2000);
          BankDetailPage.bankCodeForSE.setValue(testData[country]['bankCode']);
          browser.pause(5000);
          browser.waitForVisible(continueButton);
          break;
        case 'br':
          browser.waitForVisible(paymentTypeforBROption);
          BankDetailPage.paymentTypeforBR.click();
          BankDetailPage.bankCodeForSE.selectByIndex(1);
          BankDetailPage.branchCodeForBR.setValue('1234');
          BankDetailPage.accounTypeForBR.selectByIndex(2);
          BankDetailPage.accountSubtypeForBR.selectByIndex(2);
          BankDetailPage.accountNumberForBR.setValue('123456');
          BankDetailPage.checkDigitForBR.setValue('7');
          break;
      }
      for (var k in h) {
          if (h.hasOwnProperty(k)) {
              if(k==country){
                browser.waitForVisible(actIban);
                BankDetailPage.iban.setValue(testData[country]['IBAN']);
                browser.pause(3000);
                browser.clearElement(swift);
                BankDetailPage.bic.setValue(testData[country]['BIC']);
            }
          }
      }
      if (country == 'br') {
        browser.pause(2000);
        browser.waitForVisible(continueForBRButton);
        BankDetailPage.continueForBR.click();
        browser.waitForVisible(okButton);
        BankDetailPage.okBut.click();
      } else {
        browser.waitForVisible(continueButton);
        browser.pause(3000);
        BankDetailPage.saveAndContinue.click();
        browser.waitForVisible(okButton);
        BankDetailPage.okBut.click();
      }
    }
  }
});

module.exports = BankDetailPage;
