

# End-2-end Automation

Automated test to validate the account sign up and purchase process.

## Features

#### 1. Multiple Environment:
Tests can be configured to run in different environments.
For example: In beta, staging and production.

#### 2. Multi-Browser Support:
Tests can be configured to run in chrome and firefox browsers.

#### 3. Parallel Execution:
Multiple tests can be configured to run in parallel to save execution time.

#### 4. Integrated with Browserstack:
Test can run in different browsers with different OS platform.

## Prerequisites

* Node JS (https://nodejs.org/en/download/)

## Usage
Clone the repo and go to e2e folder and install the node_modules using the command 'npm install'.

## How-To Run locally:

* To run in beta:
  `   npm run beta`
* To run in staging:
  `   npm run staging`
* To run in prod:
  `   npm run prod`
* To run a specific suite:(for example to run all the test inside pay_with_bankwire)
  `  wdio wdio.{environment}.conf.js --suite pay_with_bankwire` where environment can be beta/staging/prod
* To run a specific test:
  `  ./node_modules/.bin/wdio wdio.{environment}.conf.js --spec ./test/specs/endToEnd/signup/pay_with_cc/iDEAL_nl.js` where environment can be beta/staging/prod

Note: Brazil test can not be run in parallel because cpf and cnpj are generated one at a time while executing the test.

## How-To Run using docker:

* To start the containers:
  `   docker-compose up -d`

* Run the test as mentioned above

* To stop the containers:
`   docker-compose down`

* To scale up the browsers :
`   docker-compose scale chromenode=4`

## How-To Run using Browserstack:
`   npm run bs`

## Tech Stack

* JavaScript
* Mocha-Chai
* WebDriver I/O
