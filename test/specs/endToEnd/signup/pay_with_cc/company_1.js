/*global require, describe, it, browser,before, after, beforeEach, exist*/
var Page = require('../../../../../pageobjects/page');
var expect = require('chai').expect;
var CreateAccountPage = require('../../../../../pageobjects/create_account.page');
var ShippingDetailsPage = require('../../../../../pageobjects/shipping_details.page');
var BusinessDetailPage = require('../../../../../pageobjects/business_details.page');
var PersonalDetailPage = require('../../../../../pageobjects/personal_details.page');
var LoginPage = require('../../../../../pageobjects/login.page');
var BankDetailPage = require('../../../../../pageobjects/bank_details.page');
var dbconfig_beta = require('../../../dbconfig/dbconfig_beta');
var DBconfigForStaging = require('../../../dbconfig/dbconfig_staging');
var AccountActivationPage = require('../../../../../pageobjects/account_activation.page');
var DBValidation = require('../../../../../pageobjects/db_validation');
var ReadFile = require('fs');
var contents = ReadFile.readFileSync('test/specs/data/test_account.json');

var createAccountLink = '.pre-auth__footer-right a.ng-binding';
var continueOnCreateAccountBt =
  '[ng-click="dialogue.handleCountry(dialogue.country)"]';

let locale = 'en-it';
let testData = JSON.parse(contents);
let country = locale.split('-')[1];
let account;

var localeList = [{"locale" : "en-it"},{"locale" : "en-lv"},{"locale" : "en-lt"},{"locale" : "en-no"},{"locale" : "en-nl"}];

('use strict');
describe('Account creation and activation', function() {

  function createAccount(localeObject) {
    var country = localeObject.locale.split('-')[1];
    console.log('country' + country);

  it('should be able to go to login page for '+localeObject.locale, function() {
    browser.url('/' + `${localeObject.locale}` + '/signup/create-account');
    browser.windowHandleSize({ width: 1300, height: 768 });
  });
  it('should be able to enter username and password and create account', function() {
    account = CreateAccountPage.getAccount(country);
    CreateAccountPage.fillEmailAndPassword(account, account);
    browser.pause(2000);
    CreateAccountPage.privacyPolicy.click();
    CreateAccountPage.continue.click();
  });
  it('should be able to fill shipping details and select bankwire as payment method', function() {
    ShippingDetailsPage.fillShippingDetails(country, testData);
    ShippingDetailsPage.selectAndFillCreditCardDetails();
    ShippingDetailsPage.clickConfirmOrder();
  });
  it('should be able to fill business details and select company', function() {
    BusinessDetailPage.addBusinessDetails();
    BusinessDetailPage.selectCompany();
    BusinessDetailPage.fillBusinessDetailsForCompany(country, testData);
    BusinessDetailPage.clikSaveAndContinue();
  });
  it('should be able to fill personal details', function() {
    PersonalDetailPage.fillPersonalDetails(country, testData);
    PersonalDetailPage.changeAddress.click();
    PersonalDetailPage.fillAddressDetails(country, testData);
    PersonalDetailPage.clickSaveAndContinue();
    BankDetailPage.fillBankDetails(country, testData);
  });
  it('should be able to activate account', async function() {
    AccountActivationPage.enterPhoneNumber(testData[country]["activatePhoneNumber"]);
    await DBValidation.inSOAP(country, account);
  });
}
for (var i = 0; i < localeList.length; i++) {
  var localeObject = localeList[i];
  console.log('localeObject' + localeObject);
  createAccount(localeObject);
}
});
