/*global require, describe, it, browser,before, after, beforeEach, exist*/

var Page = require('../../../../../pageobjects/page');
var expect = require('chai').expect;
var CreateAccountPage = require('../../../../../pageobjects/create_account.page');
var ShippingDetailsPage = require('../../../../../pageobjects/shipping_details.page');
var BusinessDetailPage = require('../../../../../pageobjects/business_details.page');
var PersonalDetailPage = require('../../../../../pageobjects/personal_details.page');
var BankDetailPage = require('../../../../../pageobjects/bank_details.page');
var AccountActivationPage = require('../../../../../pageobjects/account_activation.page');
var DBValidation = require('../../../../../pageobjects/db_validation');
var DBconfig = require('../../../dbconfig/dbconfig_staging');
var ReadFile = require('fs');
var contents = ReadFile.readFileSync('test/specs/data/test_account.json');

var localeList = [{"locale" : "en-it"},{"locale" : "en-lv"},{"locale" : "en-lt"},{"locale" : "en-no"}];//{"locale" : "en-nl"},
var testData = JSON.parse(contents);

('use strict');
describe('Account creation and activation', function() {

  function createAccount(localeObject) {
  it('and user should able to order a terminal using bankwire as sole traders for '+localeObject.locale, async function() {
    var country = localeObject.locale.split('-')[1];
    console.log('country' + country);
      browser.url('/' + `${localeObject.locale}` + '/signup/create-account');
      browser.windowHandleSize({ width: 1300, height: 768 });
      var account = CreateAccountPage.getAccount(country);
      console.log('account' + account);
      CreateAccountPage.fillEmailAndPassword(account, account);
      browser.pause(2000);
      CreateAccountPage.privacyPolicy.click();
      browser.pause(2000);
      CreateAccountPage.continue.click();
      ShippingDetailsPage.fillShippingDetails(country, testData);
      ShippingDetailsPage.selectAndFillBankWire();
      ShippingDetailsPage.clickConfirmOrder();
      BusinessDetailPage.addBusinessDetails();
      BusinessDetailPage.selectSoleTraders();
      BusinessDetailPage.fillBusinessDetailsForSoleTraders(country, testData);
      BusinessDetailPage.clikSaveAndContinue();
      PersonalDetailPage.fillPersonalDetails(country, testData);
      PersonalDetailPage.clickSaveAndContinue();
      BankDetailPage.fillBankDetails(country, testData);
      AccountActivationPage.enterPhoneNumber(testData[country]["activatePhoneNumber"]);
      await DBValidation.inSOAP(country, account);
  });}

  for (var i = 0; i < localeList.length; i++) {
    var localeObject = localeList[i];
    console.log('localeObject' + localeObject);
    createAccount(localeObject);
  }
});
