/*global require, describe, it, browser,before, after, beforeEach, exist*/

var Page = require('../../../../../pageobjects/page');
var expect = require('chai').expect;
var CreateAccountPage = require('../../../../../pageobjects/create_account.page');
var ShippingDetailsPage = require('../../../../../pageobjects/shipping_details.page');
var BusinessDetailPage = require('../../../../../pageobjects/business_details.page');
var PersonalDetailPage = require('../../../../../pageobjects/personal_details.page');
var BankDetailPage = require('../../../../../pageobjects/bank_details.page');
var ShopPage = require('../../../../../pageobjects/shop.page');
var OverviewPage = require('../../../../../pageobjects/overview.page');
var fs = require('fs');
var contents = fs.readFileSync('test/specs/data/test_account.json');

describe('When user go to create account page', function() {
  'use strict';
  var locale = 'en-br';
  var testData = JSON.parse(contents);
  var country = locale.split('-')[1];
  var account = CreateAccountPage.getAccount(country);

  describe('And fill valid details', function() {
    beforeEach(function() {
      browser.url('/' + `${locale}` + '/signup/create-account');
      browser.windowHandleSize({ width: 1500, height: 768 });
      CreateAccountPage.fillEmailAndPassword(account, account);
      CreateAccountPage.continue.click();
    });

    it('should able to order a terminal', function() {
      browser.waitForVisible('.btn.btn--highlight._6bcd4b2281f5678313f55cbf20c36cc9._4aec7908310ea49cebf2d9ae819f818c');
      ShippingDetailsPage.addOrderToCartForTop.click();
      ShippingDetailsPage.selectSoleTraders();
      ShippingDetailsPage.selectBoleto();
      ShippingDetailsPage.fillShippingDetails(country, testData);
      ShippingDetailsPage.clickConfirmOrder();
      BusinessDetailPage.addBusinessDetails();
      BusinessDetailPage.fillBusinessDetailsForSoleTraders(country, testData);
      BusinessDetailPage.clikSaveAndContinue();
      PersonalDetailPage.fillPersonalDetails(country, testData);
      PersonalDetailPage.clickSaveAndContinue();
      BankDetailPage.fillBankDetails(country, testData);
      OverviewPage.goToShop(locale);
      ShopPage.addProductToCartForBR();
      ShopPage.buyProductUsingBoleto();
    });
  });
});
