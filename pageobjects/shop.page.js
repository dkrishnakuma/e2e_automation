const Page = require('./page');
const ReadFile = require('fs');

const contents = ReadFile.readFileSync('test/specs/data/user_details.json');
const userData = JSON.parse(contents);

const ccPayment = 'label[for="payment-method-credit_card"]';
const bankwirePayment = 'label[for="payment-method-bankwire"]';
const continueButton =
  'button.btn.btn--highlight._8a35ff3f1f1a9f6e889f06d44e3fe25f';
const orderProductButton =
  'body > div.main.ng-scope > ui-view > layout > div.layout > div > div > new-shop > div > div > section > div:nth-child(1) > div > div > div > div:nth-child(1) > div.e8cce784c628a3856a5716d6b5131bff > button';
const cardNameText = 'input[name="cardholderName"]';
const cardNumberText = '#cardNumber';
const expirationDateDropDown = 'select[name="month"]';
const yearDropDown = 'select[name="year"]';
const sofortOption = 'label[for="payment-method-sofortbanking"]';
const verificationCodeText = 'input[name="cvv"]';
const addProductToCartButtontForBR =
  '.btn.btn--highlight._6bcd4b2281f5678313f55cbf20c36cc9';
const bankwirePaymentForBR = 'label[for="payment-method-boleto"]';
const addAirFromShopButton = '.btn.btn--highlight';
const firstNameText = '#shipping_first_name';
const lastNameText = '#shipping_last_name';
const address1Text = '#shipping_address_line1';
const cityText = '#shipping_city';
const zipcodeText = '#shipping_post_code';
const regionForCLDropDown = '#shipping_parent_region_id';
const shippingCountry ='#shipping_country';
const houseNumberText = '#shipping_house_number';
const regionText = '#shipping_region_id';
const vatIDText = 'input.input.ng-valid.input--optional';
const vatIDlabl = '.label.label--optional';
const userNamelabel = '.topnav__user-actions-name.ng-binding';
const voucherTxtt = '[name="voucherCode"]';
const voucherButton = '.btn.btn--grey';
const logoutbt = 'body > div.main.ng-scope > ui-view > layout > topnav > nav > div.topnav__right-container > ul > li:nth-child(4) > a';

const ShopPage = Object.create(Page, {
  orderProduct: {
    get: function() {
      return browser.element(orderProductButton);
    }
  },

  voucherTxt: {
    get: function () {
      return browser.element(voucherTxtt);
    }
  },

  voucherBt: {
    get: function () {
      return browser.element(voucherButton);
    }
  },

  userName: {
      get: function () {
          return browser.element(userNamelabel);
      }
  },

  logout: {
      get: function () {
          return browser.element(logoutbt);
      }
  },

  region: {
      get: function () {
          return browser.element(regionText);
      }
  },

  vatIDlabel: {
      get: function () {
          return browser.element(vatIDlabl);
      }
  },

  addAirFromShop: {
    get: function() {
      return browser.element(addAirFromShopButton);
    }
  },

  bankWire: {
    get: function() {
      return browser.element(bankwirePayment);
    }
  },

  houseNumber: {
      get: function () {
          return browser.element(houseNumberText);
      }
  },

  regionForCL: {
      get: function () {
          return browser.element(regionForCLDropDown);
      }
  },

  boleto: {
    get: function() {
      return browser.element(bankwirePaymentForBR);
    }
  },

  ccPaymentMethod: {
    get: function() {
      return browser.element(ccPayment);
    }
  },

  continue: {
    get: function() {
      return browser.element(continueButton);
    }
  },

  cardName: {
    get: function() {
      return browser.element(cardNameText);
    }
  },

  cardNumber: {
    get: function() {
      return browser.element(cardNumberText);
    }
  },

  expirationDate: {
    get: function() {
      return browser.element(expirationDateDropDown);
    }
  },

  year: {
    get: function() {
      return browser.element(yearDropDown);
    }
  },

  sofort: {
    get: function() {
      return browser.element(sofortOption);
    }
  },

  verificationCode: {
    get: function() {
      return browser.element(verificationCodeText);
    }
  },

  addProductToCartBtForBR: {
    get: function() {
      return browser.element(addProductToCartButtontForBR);
    }
  },

  firstNameFeild: {
      get: function () {
          return browser.element(firstNameText);
      }
  },

  lastNameFeild: {
      get: function () {
          return browser.element(lastNameText);
      }
  },

  address1: {
      get: function () {
          return browser.element(address1Text);
      }
  },

  city: {
      get: function () {
          return browser.element(cityText);
      }
  },

  shippingToCountry: {
      get: function () {
          return browser.element(shippingCountry);
      }
  },

  zipcode: {
      get: function () {
          return browser.element(zipcodeText);
      }
  },

  addProductToCart: {
    value: function() {
      browser.pause(2000);
      ShopPage.orderProduct.click();
    }
  },

  enterVatID: {
      get: function () {
          return browser.element(vatIDText);
      }
  },

  addProductToCartForBR: {
    value: function() {
      browser.pause(4000);
      ShopPage.addProductToCartBtForBR.click();
    }
  },

  buyProductUsingCC: {
    value: function() {
      browser.waitForVisible(ccPayment);
      console.log('Testtttttt1!!!')
      ShopPage.ccPaymentMethod.click();
      ShopPage.cardName.setValue(userData['userDetails']['cardHoldrName']);
      ShopPage.cardNumber.setValue(userData['userDetails']['ccNumber']);
      ShopPage.expirationDate.selectByIndex(1);
      ShopPage.year.selectByIndex(3);
      ShopPage.verificationCode.setValue(userData['userDetails']['code']);
      browser.waitForVisible(continueButton);
      ShopPage.continue.click();
      browser.pause(3000);
    }
  },

  enterShippingDetailsAndPayUsingCC: {
    value: function(testData,country,vatCountry) {
      browser.pause(3000);
      ShopPage.addAirFromShop.click();
      browser.waitForVisible(firstNameText);
      ShopPage.firstNameFeild.setValue(userData["userDetails"]["firstName"]);
      ShopPage.lastNameFeild.setValue(userData["userDetails"]["lastName"]);
      if(country!='br'){
      ShopPage.address1.setValue(userData["userDetails"]["address"]);
      ShopPage.city.setValue(userData["userDetails"]["city"]);
      }
      if(country=='br'){
        ShopPage.CEP.setValue(testData[country]["postalCode"]);
      } else {
        ShopPage.zipcode.setValue(testData[country]["postalCode"]);
      //  ShopPage.shippingToCountry.selectByValue(testData[country]["shippingCountry"]);
      //ShopPage.enterVatID.setValue(testData[vatCountry]["vatID"]);
      //ShopPage.vatIDlabel.click();
      //browser.pause(3000);
      }
      switch(country){
        case 'br':
        case 'nl':
          browser.waitForVisible(houseNumberText);
          ShopPage.houseNumber.setValue(userData["userDetails"]["houseNo"]);
          break;
      }
      switch(country){
        case 'it':
        case 'es':
        case 'us':
        case 'ie':
        case 'pt':
        case 'pl':
        ShopPage.region.selectByIndex(3);
        break;
        case 'cl':
        ShopPage.regionForCL.selectByIndex(3);
        ShopPage.region.selectByIndex(3);
        break;
      }
      browser.waitForVisible(ccPayment);
      ShopPage.ccPaymentMethod.click();
      ShopPage.cardName.setValue(userData['userDetails']['cardHoldrName']);
      ShopPage.cardNumber.setValue(userData['userDetails']['ccNumber']);
      ShopPage.expirationDate.selectByIndex(1);
      ShopPage.year.selectByIndex(3);
      ShopPage.verificationCode.setValue(userData['userDetails']['code']);
      browser.waitForVisible(continueButton);
      ShopPage.continue.click();
      browser.pause(3000);
    }
  },

  enterShippingDetailsAndPayUsingBankwire: {
    value: function(testData,country,vatCountry) {
      browser.pause(3000);
      ShopPage.addAirFromShop.click();
      browser.waitForVisible(firstNameText);
      ShopPage.firstNameFeild.setValue(userData["userDetails"]["firstName"]);
      ShopPage.lastNameFeild.setValue(userData["userDetails"]["lastName"]);
      if(country!='br'){
      ShopPage.address1.setValue(userData["userDetails"]["address"]);
      ShopPage.city.setValue(userData["userDetails"]["city"]);
      }
      if(country=='br'){
        ShopPage.CEP.setValue(testData[country]["postalCode"]);
      } else {
        ShopPage.zipcode.setValue(testData[country]["postalCode"]);
      //  ShopPage.shippingToCountry.selectByValue(testData[country]["shippingCountry"]);
      //ShopPage.enterVatID.setValue(testData[vatCountry]["vatID"]);
      //ShopPage.vatIDlabel.click();
      //browser.pause(3000);
      }
      switch(country){
        case 'br':
        case 'nl':
          browser.waitForVisible(houseNumberText);
          ShopPage.houseNumber.setValue(userData["userDetails"]["houseNo"]);
          break;
      }
      switch(country){
        case 'it':
        case 'es':
        case 'us':
        case 'ie':
        case 'pt':
        case 'pl':
        ShopPage.region.selectByIndex(3);
        break;
        case 'cl':
        ShopPage.regionForCL.selectByIndex(3);
        ShopPage.region.selectByIndex(3);
        break;
      }
      browser.waitForVisible(bankwirePayment);
      ShopPage.bankWire.click();
      ShopPage.continue.click();
      browser.pause(3000);
    }
  },

  enterShippingDetailsAndPayUsingSofort: {
    value: function(testData,country,vatCountry) {
      browser.pause(3000);
      ShopPage.addAirFromShop.click();
      browser.waitForVisible(firstNameText);
      ShopPage.firstNameFeild.setValue(userData["userDetails"]["firstName"]);
      ShopPage.lastNameFeild.setValue(userData["userDetails"]["lastName"]);
      if(country!='br'){
      ShopPage.address1.setValue(userData["userDetails"]["address"]);
      ShopPage.city.setValue(userData["userDetails"]["city"]);
      }
      if(country=='br'){
        ShopPage.CEP.setValue(testData[country]["postalCode"]);
      } else {
        ShopPage.zipcode.setValue(testData[country]["postalCode"]);
      //  ShopPage.shippingToCountry.selectByValue(testData[country]["shippingCountry"]);
      //ShopPage.enterVatID.setValue(testData[vatCountry]["vatID"]);
      //ShopPage.vatIDlabel.click();
      //browser.pause(3000);
      }
      switch(country){
        case 'br':
        case 'nl':
          browser.waitForVisible(houseNumberText);
          ShopPage.houseNumber.setValue(userData["userDetails"]["houseNo"]);
          break;
      }
      switch(country){
        case 'it':
        case 'es':
        case 'us':
        case 'ie':
        case 'pt':
        case 'pl':
        ShopPage.region.selectByIndex(3);
        break;
        case 'cl':
        ShopPage.regionForCL.selectByIndex(3);
        ShopPage.region.selectByIndex(3);
        break;
      }
      ShopPage.sofort.scroll();
      ShopPage.sofort.click();
      browser.pause(3000);
    }
  },

  selectSofort: {
    value: function() {
      browser.pause(1000);
      ShopPage.sofort.scroll();
      ShopPage.sofort.click();
    }
  },

  clickConfirmOrder: {
    value: function() {
      browser.waitForVisible(continueButton);
      ShopPage.continue.click();
      browser.pause(3000);
    }
  },

  applyVoucher: {
    value: function () {
      browser.waitForVisible(voucherPlus);
      ShopPage.voucher.click();
      ShopPage.voucherTxt.setValue(userData["userDetails"]["voucherCode"]);
      ShopPage.voucherBt.click();
     }
   },

  buyProductUsingBoleto: {
    value: function() {
      browser.pause(3000);
      browser.waitForVisible(bankwirePaymentForBR)
      ShopPage.boleto.click();
      ShopPage.continue.click();
      browser.pause(3000);
    }
  },

  buyProductUsingBankWire: {
    value: function() {
      browser.waitForVisible(bankwirePayment);
      ShopPage.bankWire.click();
      ShopPage.continue.click();
    }
  }
});

module.exports = ShopPage;
