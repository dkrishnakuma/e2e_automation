/*global require, describe, it, browser,before, after, beforeEach, exist*/

var Page = require('../../../../../pageobjects/page');
var expect = require('chai').expect;
var CreateAccountPage = require('../../../../../pageobjects/create_account.page');
var ShippingDetailsPage = require('../../../../../pageobjects/shipping_details.page');
var BusinessDetailPage = require('../../../../../pageobjects/business_details.page');
var AccountActivationPage = require('../../../../../pageobjects/account_activation.page');
var PersonalDetailPage = require('../../../../../pageobjects/personal_details.page');
var BankDetailPage = require('../../../../../pageobjects/bank_details.page');
var OverviewPage = require('../../../../../pageobjects/overview.page');
var DBconfig = require('../../../dbconfig/dbconfig_staging');
var ShopPage = require('../../../../../pageobjects/shop.page');
var fs = require('fs');
var contents = fs.readFileSync('test/specs/data/test_account.json');

describe('When user go to create account page', function() {
  'use strict';
  var locale = 'en-br';
  var testData = JSON.parse(contents);
  var country = locale.split('-')[1];
  var account = CreateAccountPage.getAccount(country);

  describe('And fill valid details', function() {
    beforeEach(function() {
      browser.url('/' + `${locale}` + '/signup/create-account');
      browser.windowHandleSize({ width: 1500, height: 768 });
      CreateAccountPage.fillEmailAndPassword(account, account);
      CreateAccountPage.continue.click();
    });

    it('skip purchase and register account as sole trader for BR', function() {
      ShippingDetailsPage.skipPurchase();
      BusinessDetailPage.selectSoleTraders();
      BusinessDetailPage.fillBusinessDetailsForSoleTraders(country, testData);
      BusinessDetailPage.fillPostalCodeAndPhoneNumber(country, testData);
      BusinessDetailPage.clikSaveAndContinue();
      PersonalDetailPage.fillPersonalDetails(country, testData);
      PersonalDetailPage.fillCEP();
      PersonalDetailPage.clickSaveAndContinue();
      BankDetailPage.fillBankDetails(country, testData);
      OverviewPage.goToShop(locale);
      ShopPage.addProductToCartForBR();
      ShopPage.buyProductUsingBoleto();
    });
  });
});
