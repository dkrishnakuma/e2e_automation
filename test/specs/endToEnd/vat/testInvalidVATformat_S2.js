/*global require, describe, it, browser,before, after, beforeEach, exist*/

var Page = require('../../../../pageobjects/page');
var expect = require('chai').expect;
var CreateAccountPage = require('../../../../pageobjects/create_account.page');
var ShippingDetailsPage = require('../../../../pageobjects/shipping_details.page');
var BusinessDetailPage = require('../../../../pageobjects/business_details.page');
var PersonalDetailPage = require('../../../../pageobjects/personal_details.page');
var BankDetailPage = require('../../../../pageobjects/bank_details.page');
var AccountActivationPage = require('../../../../pageobjects/account_activation.page');
var DBValidation = require('../../../../pageobjects/db_validation');
var DBconfig = require('../../dbconfig/dbconfig_beta');
var ShopPage = require('../../../../pageobjects/shop.page');
var ReadFile = require('fs');
var contents = ReadFile.readFileSync('test/specs/data/vat.json');
var testdata = ReadFile.readFileSync('test/specs/data/test_account.json');

//var localeList = [{"locale" : "en-at"}];
var localeList = [{"locale" : "en-at"},{"locale" : "en-be"},{"locale" : "en-cy"},{"locale" : "en-cz"}
,{"locale" : "en-fi"},{"locale" : "en-fr"},{"locale" : "en-hu"},{"locale" : "en-it"}
,{"locale" : "en-lt"},{"locale" : "en-nl"},{"locale" : "en-pl"},{"locale" : "en-pt"},{"locale" : "en-si"},{"locale" : "en-es"}
,{"locale" : "en-se"},{"locale" : "en-gb"},{"locale" : "en-dk"},{"locale" : "en-ee"},{"locale" : "en-lu"},{"locale" : "en-mt"}
,{"locale" : "en-sk"}];
var shippingData = JSON.parse(contents);
var testInfo = JSON.parse(testdata);
var account;

var shipList = [{"ship" : "fr"}];//,{"ship" : "de"},{"ship" : "fr"},{"ship" : "de"}];
var vatList = [{"vat" : "in"}];//,{"vat" : "fr"}];

('use strict');
describe('Account creation and activation', async function() {

  function testScenario(shipCountry,vatCountry,localeObject) {
    it('should be able to go to create account page and create account for '+localeObject.locale, async function() {
      var country = localeObject.locale.split('-')[1];
       browser.url('/' + `${localeObject.locale}` + '/signup/create-account');
       browser.windowHandleSize({ width: 1300, height: 768 });
       account =  CreateAccountPage.getAccount(country);
       CreateAccountPage.fillEmailAndPassword(account, account);
       CreateAccountPage.continue.click();
       ShippingDetailsPage.fillShippingDetailsForVAT(shipCountry.ship,shippingData,vatCountry.vat);
       ShippingDetailsPage.selectAndFillCreditCardDetails();
       ShippingDetailsPage.clickConfirmOrder();
       ShippingDetailsPage.goToShop(localeObject.locale);
       ShopPage.enterShippingDetailsAndPayUsingCC(testInfo,country,vatCountry.vat);
       ShopPage.userName.click();
       ShopPage.logout.click();
       await DBValidation.vatValidationWithoutVatID(country, account);
    });
  }

  for (var i = 0; i < localeList.length; i++) {
    var localeObject = localeList[i];
    console.log('localeObject' + localeObject.locale);
    var shipCountry = shipList[0];
    var vatCountry = vatList[0];
    console.log('shipCountry' + shipCountry.ship)
    testScenario(shipCountry,vatCountry,localeObject);
  }
});
