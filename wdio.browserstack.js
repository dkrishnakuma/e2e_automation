// Let's load the default configs:
var defaults = require("./wdio.conf.js").config;

var _ = require("lodash");

var overrides = {
    baseUrl: "https://dashboard.sam-app.ro/",
    capabilities: [{
    browser: 'Edge',
    platform: 'WINDOWS'
    },{
    browser: 'internet explorer',
    platform: 'WINDOWS'
    }],
    /*{
    browser: 'safari',
    platform: 'MAC'
  }],*/
    maxInstances: 2,
    user: "clmentluton1",
    key: "baBn1n2pTfERqGxHxf3p",
    services: ['browserstack']
};

// Send the merged config to wdio
exports.config = _.defaultsDeep(overrides, defaults);
