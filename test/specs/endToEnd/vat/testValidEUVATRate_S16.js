/*global require, describe, it, browser,before, after, beforeEach, exist*/

var Page = require('../../../../pageobjects/page');
var expect = require('chai').expect;
var CreateAccountPage = require('../../../../pageobjects/create_account.page');
var ShippingDetailsPage = require('../../../../pageobjects/shipping_details.page');
var BusinessDetailPage = require('../../../../pageobjects/business_details.page');
var PersonalDetailPage = require('../../../../pageobjects/personal_details.page');
var BankDetailPage = require('../../../../pageobjects/bank_details.page');
var AccountActivationPage = require('../../../../pageobjects/account_activation.page');
var DBValidation = require('../../../../pageobjects/db_validation');
var ShopPage = require('../../../../pageobjects/shop.page');
var DBconfig = require('../../dbconfig/dbconfig_beta');
var ReadFile = require('fs');
var contents = ReadFile.readFileSync('test/specs/data/vat.json');
var testdata = ReadFile.readFileSync('test/specs/data/test_account.json');

//var localeList = [{"locale" : "en-fr"}];//bg, ie, lv
var localeList = [{"locale" : "en-de"}];
var shippingData = JSON.parse(contents);
var testInfo = JSON.parse(testdata);
//var vatList = [{"vat" : "fr"}];
var vatList = [{"vat" : "at"},{"vat" : "be"},{"vat" : "cy"},{"vat" : "cz"}
,{"vat" : "fi"},{"vat" : "fr"},{"vat" : "hu"},{"vat" : "it"}
,{"vat" : "lt"},{"vat" : "nl"},{"vat" : "pl"},{"vat" : "pt"},{"vat" : "si"},{"vat" : "es"}
,{"vat" : "se"},{"vat" : "gb"},{"vat" : "dk"},{"vat" : "ee"},{"vat" : "lu"},{"vat" : "mt"}
,{"vat" : "sk"}];

var account;

//var shipList = [{"ship" : "fr"}];
var shipList = [{"ship" : "de"}];

('use strict');
describe('Account creation and activation', async function() {

  function testScenario(shipCountry,localeObject,vatCountry) {
    it('should be able to go to create account page and create account for '+localeObject.locale, async function() {
      var country = localeObject.locale.split('-')[1];
       browser.url('/' + `${localeObject.locale}` + '/signup/create-account');
       browser.windowHandleSize({ width: 1300, height: 768 });
       account =  CreateAccountPage.getAccount(country);
       CreateAccountPage.fillEmailAndPassword(account, account);
       CreateAccountPage.continue.click();
       ShippingDetailsPage.fillShippingDetailsForVAT(shipCountry.ship,shippingData,vatCountry.vat);
       ShippingDetailsPage.selectAndFillCreditCardDetails();
       ShippingDetailsPage.clickConfirmOrder();
       ShippingDetailsPage.goToShop(localeObject.locale);
       ShopPage.enterShippingDetailsAndPayUsingCC(testInfo,country);
       ShopPage.userName.click();
       ShopPage.logout.click();
       await DBValidation.vatValidationForDE(country, account);
    });
  }

  for (var i = 0; i < vatList.length; i++) {
    var localeObject = localeList[0];
    console.log('localeObject' + localeObject.locale);
    var shipCountry = shipList[0];
    var vatCountry = vatList[i];
    console.log('shipCountry' + shipCountry.ship)
    testScenario(shipCountry,localeObject,vatCountry);
  }
});
