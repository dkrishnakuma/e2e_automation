'use strict';
var Page = require('./page');
var expect = require('chai').expect;
var AccountActivationPage = require('./account_activation.page');
var DBconfigForStaging = require('../test/specs/dbconfig/dbconfig_staging');
var DBconfigForBeta = require('../test/specs/dbconfig/dbconfig_beta');
var DBconfigForProd = require('../test/specs/dbconfig/dbconfig_prod');
var DBconfigForTheta = require('../test/specs/dbconfig/dbconfig_theta');

var codeBtt = '.css-yvbbfo-ChildrenWrapper-childrenWrapperStyles';
var codeTxt = '#requestCode';

//var codeBtt = '[ng-click="smsCodeVerification.submit(smsCodeVerification.code, SmsCodeVerificationForm)"]';
//var codeTxt = '#code';

var DBValidation = Object.create(Page, {
  inSOAP: {
      value: async function (country,account) {
        var url = await browser.getUrl();
        var envBeforeParse = url.split('//')[1];
        var env = envBeforeParse.split('.')[0];
        console.log('env'+env);
        var knex;
        if(env=='dashboard-beta')
        {
          knex = require('knex')(DBconfigForBeta);
        } else if (env=='dashboard') {
         knex = require('knex')(DBconfigForStaging);
       } else if  (env=='me') {
         knex = require('knex')(DBconfigForProd);
       } else {
          knex = require('knex')(DBconfigForTheta);
        }
        var subquery = knex('users').where('email', '=', account).select('merchant_id');
        var res = await knex('activations').where('merchant_id', 'in', subquery).select('code');
        await browser.pause(5000);
        await browser.setValue(codeTxt, AccountActivationPage.parseString(res));
        await browser.pause(3000);
        await browser.click(codeBtt);
        await browser.pause(5000);
        var transactionLimitBeforeParse = await knex('merchant_limits').where('merchant_id', 'in', subquery).select('transaction_limit');
        await console.log(AccountActivationPage.parseNumeric(transactionLimitBeforeParse));
        var settlementLimitBeforeParse = await knex('merchant_limits').where('merchant_id', 'in', subquery).select('settlement_limit');
        await console.log(AccountActivationPage.parseNumeric(settlementLimitBeforeParse));
        var activationStatus = await knex('merchant_account_statuses').where('merchant_id', 'in', subquery).select('state');
        await console.log(AccountActivationPage.parseString(activationStatus));

        switch (country) {
          case 'at':
          case 'cy':
          case 'fi':
          case 'fr':
          case 'de':
          case 'ie':
          case 'it':
          case 'lv':
          case 'lt':
          case 'pt':
          case 'si':
          case 'es':
          case 'ch':
          case 'ee':
          case 'lu':
          case 'gb':
          case 'mt':
          case 'sk':
            await expect('500000').to.equal(AccountActivationPage.parseNumeric(transactionLimitBeforeParse));
            await expect('500000').to.equal(AccountActivationPage.parseNumeric(settlementLimitBeforeParse));
            await expect("ACTIVE").to.equal(AccountActivationPage.parseString(activationStatus));
          break;
          case 'bg':
            await expect('1000000').to.equal(AccountActivationPage.parseNumeric(transactionLimitBeforeParse));
            await expect('1000000').to.equal(AccountActivationPage.parseNumeric(settlementLimitBeforeParse));
            await expect("ACTIVE").to.equal(AccountActivationPage.parseString(activationStatus));
          break;
          case 'cz':
            await expect('13000000').to.equal(AccountActivationPage.parseNumeric(transactionLimitBeforeParse));
            await expect('13000000').to.equal(AccountActivationPage.parseNumeric(settlementLimitBeforeParse));
            await expect("ACTIVE").to.equal(AccountActivationPage.parseString(activationStatus));
          break;
          case 'hu':
            await expect('150000000').to.equal(AccountActivationPage.parseNumeric(transactionLimitBeforeParse));
            await expect('150000000').to.equal(AccountActivationPage.parseNumeric(settlementLimitBeforeParse));
            await expect("ACTIVE").to.equal(AccountActivationPage.parseString(activationStatus));
          break;
          case 'no':
          case 'se':
            await expect('5000000').to.equal(AccountActivationPage.parseNumeric(transactionLimitBeforeParse));
            await expect('5000000').to.equal(AccountActivationPage.parseNumeric(settlementLimitBeforeParse));
            await expect("ACTIVE").to.equal(AccountActivationPage.parseString(activationStatus));
          break;
          case 'pl':
            await expect('2000000').to.equal(AccountActivationPage.parseNumeric(transactionLimitBeforeParse));
            await expect('2000000').to.equal(AccountActivationPage.parseNumeric(settlementLimitBeforeParse));
            await expect("ACTIVE").to.equal(AccountActivationPage.parseString(activationStatus));
          break;
          case 'dk':
            await expect('4000000').to.equal(AccountActivationPage.parseNumeric(transactionLimitBeforeParse));
            await expect('4000000').to.equal(AccountActivationPage.parseNumeric(settlementLimitBeforeParse));
            await expect("ACTIVE").to.equal(AccountActivationPage.parseString(activationStatus));
          break;
          case 'cl':
            await expect('100000000').to.equal(AccountActivationPage.parseNumeric(transactionLimitBeforeParse));
            await expect('100000000').to.equal(AccountActivationPage.parseNumeric(settlementLimitBeforeParse));
            await expect("ACTIVE").to.equal(AccountActivationPage.parseString(activationStatus));
          break;
        }
      }
    },
    vatValidation: {
        value: async function (country,account) {
    const knex = require('knex')(DBconfigForStaging);
    var subquery = knex('users').where('email', '=', account).select('merchant_id');
    var res1 = await knex('shipping_orders').where('merchant_id', 'in', subquery).select('vat_rate');
    var beforeParseCode = JSON.stringify(res1);
    var parsedCode = beforeParseCode.split('"')[3];
    console.log('Output of db:'+parsedCode);
    await expect('0.00').to.equal(parsedCode);
    }
  },

    gdprValidationForBoth: {
        value: async function (country,account) {
    const knex = require('knex')(DBconfigForBeta);
    var subquery = knex('users').where('email', '=', account).select('id');
    var res1 = await knex('gdpr_agreements').where('user_id', 'in', subquery).select('pp_tc','marketing');
    var beforeParseCode = JSON.stringify(res1);
    var parsedCode = beforeParseCode.split(',');
    console.log('Output of db:'+beforeParseCode);
    await expect('[{"pp_tc":true').to.equal(beforeParseCode.split(',')[0]);
    await expect('"marketing":true}]').to.equal(beforeParseCode.split(',')[1]);
    }
  },

  gdprValidationForOne: {
      value: async function (country,account) {
  const knex = require('knex')(DBconfigForBeta);
  var subquery = knex('users').where('email', '=', account).select('id');
  var res1 = await knex('gdpr_agreements').where('user_id', 'in', subquery).select('pp_tc','marketing');
  var beforeParseCode = JSON.stringify(res1);
  var parsedCode = beforeParseCode.split(',');
  console.log('Output of db:'+beforeParseCode);
  await expect('[{"pp_tc":true').to.equal(beforeParseCode.split(',')[0]);
  await expect('"marketing":false}]').to.equal(beforeParseCode.split(',')[1]);
  }
},

  vatValidationForDE: {
      value: async function (country,account) {
  const knex = require('knex')(DBconfigForStaging);
  var subquery = knex('users').where('email', '=', account).select('merchant_id');
  var res1 = await knex('shipping_orders').where('merchant_id', 'in', subquery).select('vat_rate');
  var beforeParseCode = JSON.stringify(res1);
  var parsedCode = beforeParseCode.split('"')[3];
  console.log('Output of db:'+parsedCode);
  await expect('19.00').to.equal(parsedCode);
  }
},

voucherValidationForBE: {
    value: async function (country,account) {
const knex = require('knex')(DBconfigForBeta);
var subquery = knex('users').where('email', '=', account).select('merchant_id');
var res1 = await knex('shipping_orders').where('merchant_id', 'in', subquery).select('price_with_vat');
var beforeParseCode = JSON.stringify(res1);
var parsedCode = beforeParseCode.split('"')[3];
console.log('Output of db:'+parsedCode);
await expect('95.59').to.equal(parsedCode);
}
},

voucherValidationForNL: {
    value: async function (country,account) {
const knex = require('knex')(DBconfigForBeta);
var subquery = knex('users').where('email', '=', account).select('merchant_id');
var res1 = await knex('shipping_orders').where('merchant_id', 'in', subquery).select('price_with_vat');
var beforeParseCode = JSON.stringify(res1);
var parsedCode = beforeParseCode.split('"')[3];
console.log('Output of db:'+parsedCode);
await expect('95.59').to.equal(parsedCode);
}
},

voucherValidationForAU: {
    value: async function (country,account) {
const knex = require('knex')(DBconfigForBeta);
var subquery = knex('users').where('email', '=', account).select('merchant_id');
var res1 = await knex('shipping_orders').where('merchant_id', 'in', subquery).select('price_with_vat');
var beforeParseCode = JSON.stringify(res1);
var parsedCode = beforeParseCode.split('"')[3];
console.log('Output of db:'+parsedCode);
await expect('56.88').to.equal(parsedCode);
}
},

voucherValidationForDE: {
    value: async function (country,account) {
const knex = require('knex')(DBconfigForBeta);
var subquery = knex('users').where('email', '=', account).select('merchant_id');
var res1 = await knex('shipping_orders').where('merchant_id', 'in', subquery).select('price_with_vat');
var beforeParseCode = JSON.stringify(res1);
var parsedCode = beforeParseCode.split('"')[3];
console.log('Output of db:'+parsedCode);
await expect('56.41').to.equal(parsedCode);
}
},

getUserID: {
    value: async function (account) {
const knex = require('knex')(DBconfigForBeta);
//var subquery = knex('users').where('email', '=', account).select('merchant_id');
var res1 = await knex('users').where('email', 'in', account).select('id');
var beforeParseCode = JSON.stringify(res1);
//var parsedCode = beforeParseCode.split('"')[3];
console.log('Output of db:'+beforeParseCode);
//await expect('56.41').to.equal(parsedCode);
}
},

  vatValidationWithoutVatID: {
      value: async function (country,account) {
  const knex = require('knex')(DBconfigForStaging);
  var subquery = knex('users').where('email', '=', account).select('merchant_id');
  var res1 = await knex('shipping_orders').where('merchant_id', 'in', subquery).select('vat_rate');
  var beforeParseCode = JSON.stringify(res1);

  console.log('Output of RES:'+beforeParseCode);
  var parsedCode = beforeParseCode.split('"')[3];
  console.log('Output of db:'+parsedCode);
  //await expect('0.00').to.equal(parsedCode);
  switch (country) {
    case 'at':
    case 'fr':
    case 'gb':
    await expect('20.00').to.equal(parsedCode);
    break;
    case 'be':
    case 'nl':
    case 'es':
    await expect('21.00').to.equal(parsedCode);
    break;
    case 'bg':
    case 'cy':
    case 'cz':
    case 'fi':
    case 'de':
    case 'gr':
    case 'hu':
    case 'lv':
    case 'lt':
    case 'si':
    case 'se':
    case 'dk':
    case 'ee':
    case 'lu':
    case 'mt':
    case 'sk':
    await expect('19.00').to.equal(parsedCode);
    break;
    case 'ie':
    case 'pl':
    case 'pt':
    await expect('23.00').to.equal(parsedCode);
    break;
    await expect('22.00').to.equal(parsedCode);
    break;
  }
  }
}
});

  module.exports = DBValidation;
