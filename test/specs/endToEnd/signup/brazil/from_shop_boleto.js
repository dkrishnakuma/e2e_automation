/*global require, describe, it, browser,before, after, beforeEach, exist*/

var Page = require('../../../pageobjects/page');
var expect = require('chai').expect;
var LoginPage = require('../../../pageobjects/login.page');
var ShopPage = require('../../../pageobjects/shop.page');
var OverviewPage = require('../../../pageobjects/overview.page');
var SofortPage = require('../../../pageobjects/sofort.page');

describe('When user go to create account page', function() {
  'use strict';
  var localePath = 'en-de';
  var locale = localePath.split('-')[1];

  describe('And fill valid details', function() {
    beforeEach(function() {
      browser.url('/' + `${localePath}` + '/login');
      browser.windowHandleSize({ width: 1500, height: 768 });
    });
    it('should able to order a terminal', function() {
      var account = 'test.account+1509827871780@example.com';
      LoginPage.performLogin(account, account);
      OverviewPage.selectShopFromOverviewPage();
      browser.pause(2000);

      /*it('hover the element and click on it', function (){
              browser.execute(function(){
                browser.pause(4000);
                $('body > div.main.ng-scope > ui-view > layout > div.layout > div > div > new-shop > div > div > div > div > div > div > div:nth-child(1) > div.e8cce784c628a3856a5716d6b5131bff > button').trigger("mouseover");
                $('body > div.main.ng-scope > ui-view > layout > div.layout > div > div > new-shop > div > div > div > div > div > div > div:nth-child(1) > div.e8cce784c628a3856a5716d6b5131bff > button').click();
              });
            });
            browser.pause(4000);
            ShopPage.addProductToCart();
            ShopPage.selectSofort();
            ShopPage.clickConfirmOrder();
            SofortPage.cancel();*/
    });
  });
});
