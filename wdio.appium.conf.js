exports.config = {
    port: 4723,

    specs: [
        './test/specs/**/order*.js'
    ],
    // Patterns to exclude.
    exclude: [
        // 'path/to/excluded/files'
    ],
    maxInstances: 3,

    capabilities: [{
        browserName: 'chrome',
        platformName: 'Android',
        appiumVersion: '1.7.1',
        platformVersion: '8.0.0',
        deviceName: 'Nexus_5X_API_26',
        orientation: 'PORTRAIT',
    }],
    /*capabilities: [{
        browserName: 'iOS',
        platformName: 'iOS',
        appiumVersion: '1.7.1',
        platformVersion: '11.0',
        deviceName: 'iPhone Simulators',
        automationName: 'XCUITest',
        newCommandTimeout: 180,
        autoAcceptAlerts: true,
        safariAllowPopups: true,
        UDID: '123123124',
        orientation: 'PORTRAIT'
    }],*/
    sync: true,
    logLevel: 'verbose',
    coloredLogs: true,
    bail: 0,
    screenshotPath: './errorShots/',
    baseUrl: 'https://dashboard-beta.sam-app.ro',
    waitforTimeout: 10000,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    services: ['appium'],
    appium: {
        args: {
            address: '127.0.0.1',
            commandTimeout: '7200',
            sessionOverride: true,
            debugLogSpacing: true,
            platformVersion: '11.0',
            platformName: 'iOS',
            showIosLog: true,
            deviceName: 'iPhone Simulator',
            nativeInstrumentsLib: true,
            isolateSimDevice: true,
            udid: '1231232134',
            automationName: 'XCUITest',
            safariAllowPopups: true
        }
    },

    framework: 'mocha',
    reporters: ['dot'],
    mochaOpts: {
        ui: 'bdd',
        timeout: 20000
    }
}
