const Page = require('./page');

const cancelLk = '.footer a[href="/payment/multipay/go/abort"]';
const cancelTransactionButton = '#CancelTransaction';

const SofortPage = Object.create(Page, {
  cancelLink: {
    get: function() {
      return browser.element(cancelLk);
    }
  },

  cancelTransaction: {
    get: function() {
      return browser.element(cancelTransactionButton);
    }
  },

  cancel: {
    value: function(locale, jsonContent) {
      browser.pause(3000);
      browser.waitForVisible(cancelLk);
      browser.pause(2000);
      SofortPage.cancelLink.click();
      browser.pause(3000);
      browser.alertAccept();
      browser.pause(2000);
      SofortPage.cancelTransaction.click();
    }
  }
});

module.exports = SofortPage;
