'use strict';
var Page = require('./page');
var cancelTransactionButton = '#hpp-form-cancel';
var confirmButton = '#hpp-confirm-button-yes';
var iNGButton = '#IDEAL-INGBNL2A';
var iNGConfirmButton = '[data-i18n="submit"]';
var transactionConfirmButton = '[name="button.edit"]';

var iDEALPage = Object.create(Page, {

  cancelTransaction: {
      get: function () {
          return browser.element(cancelTransactionButton);
      }
  },

  confirm: {
      get: function () {
          return browser.element(confirmButton);
      }
  },

  selectINGBank: {
      get: function () {
          return browser.element(iNGButton);
      }
  },

  confirmINGBank: {
      get: function () {
          return browser.element(iNGConfirmButton);
      }
  },

  confirmTransaction: {
      get: function () {
          return browser.element(transactionConfirmButton);
      }
  },

  buyReaderUsingiDEAL: {
    value: function() {
      browser.waitForVisible(iNGButton);
      iDEALPage.selectINGBank.click();
      browser.pause(2000);
      iDEALPage.confirmINGBank.click();
      browser.pause(2000);
      iDEALPage.confirmTransaction.click();
      browser.pause(2000);
    }
  },

  cancel: {
      value: function () {
          browser.pause(3000);
          browser.waitForVisible(cancelTransactionButton);
          iDEALPage.cancelTransaction.click();
          iDEALPage.confirm.click();
        }
  }
  });

  module.exports = iDEALPage;
