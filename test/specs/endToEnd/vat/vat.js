/*global require, describe, it, browser,before, after, beforeEach, exist*/

var Page = require('../../../../pageobjects/page');
var expect = require('chai').expect;
var CreateAccountPage = require('../../../../pageobjects/create_account.page');
var ShippingDetailsPage = require('../../../../pageobjects/shipping_details.page');
var BusinessDetailPage = require('../../../../pageobjects/business_details.page');
var PersonalDetailPage = require('../../../../pageobjects/personal_details.page');
var BankDetailPage = require('../../../../pageobjects/bank_details.page');
var AccountActivationPage = require('../../../../pageobjects/account_activation.page');
var DBValidation = require('../../../../pageobjects/db_validation');
var DBconfig = require('../../dbconfig/dbconfig_staging');
var ReadFile = require('fs');
var contents = ReadFile.readFileSync('test/specs/data/vat.json');

var locale = 'en-at';
var shippingData = JSON.parse(contents);
var country = locale.split('-')[1];
var account;

var shipList = [{"ship" : "fr"},{"ship" : "de"}];//,{"ship" : "fr"},{"ship" : "de"}];
//var vatList = [{"vat" : "pl"},{"vat" : "fr"},{"vat" : "fr"},{"vat" : "fr"}];

('use strict');


describe('Account creation and activation', function() {

  function testScenario(shipCountry) {
    it('should be able to go to create account page', function() {

       browser.url('/' + `${locale}` + '/signup/create-account');
       browser.windowHandleSize({ width: 1300, height: 768 });
      account =  CreateAccountPage.getAccount(country);
       CreateAccountPage.fillEmailAndPassword(account, account);
       CreateAccountPage.continue.click();
       ShippingDetailsPage.fillShippingDetailsForVAT(shipCountry.ship,shippingData);
       ShippingDetailsPage.selectAndFillCreditCardDetails();
       ShippingDetailsPage.clickConfirmOrder();
    });
  }

  for (var i = 0; i < shipList.length; i++) {
    var shipCountry = shipList[i];
    //var vatCountry = vatList[i];
    console.log('shipCountry' + shipCountry.ship)
    testScenario(shipCountry);
  }
});
