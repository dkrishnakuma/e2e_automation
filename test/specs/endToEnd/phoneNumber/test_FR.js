/*global require, describe, it, browser,before, after, beforeEach, exist*/

var Page = require('../../../../pageobjects/page');
var expect = require('chai').expect;
var CreateAccountPage = require('../../../../pageobjects/create_account.page');
var ShippingDetailsPage = require('../../../../pageobjects/shipping_details.page');
var BusinessDetailPage = require('../../../../pageobjects/business_details.page');
var PersonalDetailPage = require('../../../../pageobjects/personal_details.page');
var BankDetailPage = require('../../../../pageobjects/bank_details.page');
var AccountActivationPage = require('../../../../pageobjects/account_activation.page');
var DBValidation = require('../../../../pageobjects/db_validation');
var DBconfig = require('../../dbconfig/dbconfig_staging');
var ReadFile = require('fs');
var contents = ReadFile.readFileSync('test/specs/data/test_account.json');
var phone_content = ReadFile.readFileSync('test/specs/data/phone_number.json');

var localeList = [{"locale" : "en-fr"}];

//var phoneList = [{"phone" : "localPhoneNumber"}];
var phoneList = [{"phone" : "internationalGermanPhoneNumber"},{"phone" : "internationalGermanPhoneNumberWithZeros"},
{"phone" : "localPhoneNumber"},{"phone" : "internationalPhoneNumber"},{"phone" : "internationalPhoneNumberWithZeros"},
{"phone" : "otherCountryInternationalPhoneNumber"},{"phone" : "otherCountryInternationalPhoneNumberWithZeros"}];
//var localeList = [{"locale" : "en-at"},{"locale" : "en-cz"},{"locale" : "en-cy"}];//{"locale" : "en-bg"},{"locale" : "en-be"}
var testData = JSON.parse(contents);
var phoneData = JSON.parse(phone_content);

('use strict');
describe('Account creation and activation', function() {

  function createAccount(localeObject,phoneNumber) {
  it('and user should able to order a terminal using bankwire as sole traders for '+localeObject.locale, async function() {
    var country = localeObject.locale.split('-')[1];
    console.log('country' + country);
      browser.url('/' + `${localeObject.locale}` + '/signup/create-account');
      browser.windowHandleSize({ width: 1300, height: 768 });
      var account = CreateAccountPage.getAccount(country);
      console.log('account' + account);
      CreateAccountPage.fillEmailAndPassword(account, account);
      browser.pause(2000);
      CreateAccountPage.privacyPolicy.click();
      browser.pause(2000);
      CreateAccountPage.continue.click();
      ShippingDetailsPage.fillShippingDetailsForPhone(country, testDat, phoneData[country][phoneNumber.phone]);
      ShippingDetailsPage.selectAndFillBankWire();
      ShippingDetailsPage.clickConfirmOrder();
      BusinessDetailPage.addBusinessDetails();
      BusinessDetailPage.selectSoleTraders();
      BusinessDetailPage.fillBusinessDetailsForSoleTraders(country, testData, phoneData[country][phoneNumber.phone]);
      BusinessDetailPage.clikSaveAndContinue();
      PersonalDetailPage.fillPersonalDetails(country, testData);
      PersonalDetailPage.clickSaveAndContinue();
      BankDetailPage.fillBankDetails(country, testData);
      AccountActivationPage.enterPhoneNumber(phoneData[country][phoneNumber.phone]);
      await DBValidation.inSOAP(country, account);
  });}

  for (var i = 0; i < phoneList.length; i++) {
    var localeObject = localeList[0];
    var phoneNumber = phoneList[i];
    console.log('localeObject' + localeObject);
    createAccount(localeObject,phoneNumber);
  }
});
