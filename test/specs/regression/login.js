/*global require, describe, it, browser,before, after, beforeEach, exist*/

var Page = require('../../../pageobjects/page');
var expect = require('chai').expect;
var LoginPage = require('../../../pageobjects/login.page');
var fs = require("fs");
var contents = fs.readFileSync("test/specs/data/login.json");


describe('When user Login Sumup page', function () {

    'use strict';

    var localePath='en-fi';
    var locale=localePath.split('-')[1];
    var jsonContent = JSON.parse(contents);

    describe('And Login account', function () {
        beforeEach(function() {
          browser.url('/'+'login');
        });

        it('with invalid email id', function () {
            LoginPage.performLogin(jsonContent["invalidEmail"]["email"],jsonContent["invalidEmail"]["password"]);
            expect(browser.isVisible('[ng-show="$ctrl.loginForm.username.$error.email"]')).to.equal(true);
        });
        it('with invalid password', function () {
            LoginPage.performLogin(jsonContent["invalidPassword"]["email"],jsonContent["invalidPassword"]["password"]);
            expect(browser.isVisible('[ng-show="$ctrl.loginForm.password.$error.pattern"]')).to.equal(true);
        });
        it('with wrong email id', function () {
            LoginPage.performLogin(jsonContent["wrongEmail"]["email"],jsonContent["wrongEmail"]["password"]);
            browser.waitForVisible('[ng-show="$ctrl.loginForm.$error.INCORRECT_LOGIN"]');
            expect(browser.isVisible('[ng-show="$ctrl.loginForm.$error.INCORRECT_LOGIN"]')).to.equal(true);
        });
        it('with wrong password id', function () {
            LoginPage.performLogin(jsonContent["wrongPassword"]["email"],jsonContent["wrongPassword"]["password"]);
            browser.waitForVisible('[ng-show="$ctrl.loginForm.$error.INCORRECT_LOGIN"]');
            expect(browser.isVisible('[ng-show="$ctrl.loginForm.$error.INCORRECT_LOGIN"]')).to.equal(true);
        });
        it('with empty email id', function () {
            LoginPage.performLogin(jsonContent["emptyEmail"]["email"],jsonContent["emptyEmail"]["password"]);
            browser.waitForVisible('[ng-show="$ctrl.loginForm.$error.VALIDATION_ERROR"]');
            expect(browser.isVisible('[ng-show="$ctrl.loginForm.$error.VALIDATION_ERROR"]')).to.equal(true);
        });
        it('with empty email id', function () {
            LoginPage.performLogin(jsonContent["emptyPassword"]["email"],jsonContent["emptyPassword"]["password"]);
            browser.waitForVisible('[ng-show="$ctrl.loginForm.$error.VALIDATION_ERROR"]');
            expect(browser.isVisible('[ng-show="$ctrl.loginForm.$error.VALIDATION_ERROR"]')).to.equal(true);
        });
    });


    /*describe('Create account', function () {
        it('should able to loginto an account', function () {
            LoginPage.clickResetPassword();
            browser.waitForVisible('#username');
            expect(browser.getUrl()).to.equal(Constants.RESET_URL);
        });
    });*/

});
