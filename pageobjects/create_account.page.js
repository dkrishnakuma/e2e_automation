const Page = require('./page');

const emailId = '#email';
const passwordText = '#password';
const continueButton = 'button[type="submit"]';
const errorMessageText = '.validation-list';
const termsAndConditionLink =
  '/html/body/div[2]/ui-view/ui-view/new-signup/div/div/div[1]/div[2]/p[1]/a[1]';
const policyLink =
  '/html/body/div[2]/ui-view/ui-view/new-signup/div/div/div[1]/div[2]/p[1]/a[2]';
const termsAndConditionContentText = 'div.page.main-content.wrapper';
const loginFormLink = '.pre-auth__headline.ng-binding';
const privacyPolicyCheckbox = '.css-bl8pu2-StyledParagraph-StyledParagraph.e19avpty1';
const markettingInfoCheckbox = '//*[@id="root"]/ui-view/ui-view/new-signup/div/div/div[1]/div[2]/form/div[4]/label/p';

const CreateAccountPage = Object.create(Page, {
  getAccount: {
    value: function(country) {
      console.log(country);
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1;
      let randomNumber = Math.floor(Math.random() * 10000000000 + 1);
      var account = 'test.accounts.acquisition'+ dd + '.' + mm + '.' + country + '.' + randomNumber + '@example.com';
      return account;
    }
  },

  email: {
    get: function() {
      return browser.element(emailId);
    }
  },

  password: {
    get: function() {
      return browser.element(passwordText);
    }
  },

  continue: {
    get: function() {
      return browser.element(continueButton);
    }
  },

  errorMessage: {
    get: function() {
      return browser.element(errorMessageText);
    }
  },

  termsAndCondition: {
    get: function() {
      return browser.element(termsAndConditionLink);
    }
  },

  policy: {
    get: function() {
      return browser.element(policyLink);
    }
  },

  termsAndConditionContent: {
    get: function() {
      return browser.element(termsAndConditionContentText);
    }
  },

  loginForm: {
    get: function() {
      return browser.element(loginFormLink);
    }
  },

  privacyPolicy: {
    get: function() {
      return browser.element(privacyPolicyCheckbox);
    }
  },

  markettingInfo: {
    get: function() {
      return browser.element(markettingInfoCheckbox);
    }
  },

  fillEmailAndPassword: {
    value: function(emailAdd, password) {
      browser.waitForVisible(emailId);
      console.log('Email'+emailAdd);
      CreateAccountPage.email.setValue(emailAdd);
      CreateAccountPage.password.setValue(password);
      browser.pause(3000);
    }
  }
});

module.exports = CreateAccountPage;
