const Page = require('./../page');

const hEMVCardReader = '[tracking-id="product@topbar"]';
const hPricing = '[tracking-id="pricing@topbar"]';
const hHelp = '[tracking-id="questions@topbar"]';
const hLogin = '[tracking-id="login@topbar"]'

const fAbout = '[data-track="about@footer"]';
const fContact = '[data-track="contact@footer"]';
const fSupportCenter = '[data-track="support@footer"]';
const fPartners = '[data-track="partners@footer"]';
const fDevelopers = '[data-track="developers@footer"]';
const fAffiliate = '[data-track="affiliates@footer"]';
const fBlog = '[data-track="blog@footer"]';
const fJobs = '[data-track="jobs@footer"]';
const fPress = '[data-track="press@footer"]';
const headLineContent = '.su-hero__headline.su-hero__headline--large';
const logo = '.su-navigation__primary-logo';
const geo ='.css-1cejr4x-ButtonElement-button-button--disabled-disabledStyles-baseStyles-button--mega-sizeStyles-button--secondary-secondaryStyles.e13e8as90';

const HomePage = Object.create(Page, {

  about: {
    get: function() {
      return browser.element(fAbout);
    }
  },

  geoloc: {
    get: function() {
      return browser.element(geo);
    }
  },

  clickLogo: {
    get: function() {
      return browser.element(logo);
    }
  },

  contact: {
    get: function() {
      return browser.element(fContact);
    }
  },

  supportCenter: {
    get: function() {
      return browser.element(fSupportCenter);
    }
  },

  partners: {
    get: function() {
      return browser.element(fPartners);
    }
  },

  developers: {
    get: function() {
      return browser.element(fDevelopers);
    }
  },

  affiliate: {
    get: function() {
      return browser.element(fAffiliate);
    }
  },

  blog: {
    get: function() {
      return browser.element(fBlog);
    }
  },

  jobs: {
    get: function() {
      return browser.element(fJobs);
    }
  },

  press: {
    get: function() {
      return browser.element(fPress);
    }
  },

  headLine: {
    get: function() {
      return browser.element(headLineContent);
    }
  },

  eMVCardReader: {
    get: function() {
      return browser.element(hEMVCardReader);
    }
  },

  pricing: {
    get: function() {
      return browser.element(hPricing);
    }
  },

  help: {
    get: function() {
      return browser.element(hHelp);
    }
  },

  login: {
    get: function() {
      return browser.element(hLogin);
    }
  },

  clickEMV: {
    value: function() {
      browser.waitForVisible(hEMVCardReader);
      HomePage.eMVCardReader.click();
    }
  },

  clickPrice: {
    value: function() {
      browser.waitForVisible(hPricing);
      HomePage.pricing.click();
    }
  },

  clickHelp: {
    value: function() {
      browser.waitForVisible(hHelp);
      HomePage.help.click();
    }
  },

  clickLogin: {
    value: function() {
      browser.waitForVisible(hLogin);
      HomePage.login.click();
    }
  }
});
module.exports = HomePage;
