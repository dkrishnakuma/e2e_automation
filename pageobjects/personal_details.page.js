const Page = require('./page');
const ReadFile = require('fs');

const contents = ReadFile.readFileSync('test/specs/data/user_details.json');
const userData = JSON.parse(contents);
const cpfValue = ReadFile.readFileSync('test/specs/data/cpf.json');
const cpfData = JSON.parse(cpfValue);

const firstNameText = 'input[name="firstName"]';
const lastNameText = 'input[name="lastName"]';
const expirationDropDown = '#expiration';
const saveAndContinueButton = 'button.btn.btn--highlight.ng-binding';
const address = '#address-line1';
const monthDropDown = 'select[name="month"]';
const yearDropDown = 'select[name="year"]';
const nationalIdText = 'input[name="nationalId"]';
const nationalIdSEText = 'input[name="nationalIdSe"]';
const cityText = '#city';
const postalCodeText = '#post-code';
const changeAddressLink = 'button.link.ng-binding';
const houseNumberText = '#house_number';
const regionTxt = '#region';

var h = new Object();
h['fi'] = 1;h['fr'] = 2;h['cl'] = 3;h['es'] = 4;h['de'] = 5;h['nl'] = 6;
h['pl'] = 7;h['be'] = 8;h['ie'] = 9;h['at'] = 10;h['bg'] = 11;h['cy'] = 12;
h['cz'] = 13;h['hu'] = 14;h['lv'] = 15;h['lt'] = 16;h['no'] = 17;h['si'] = 18;
h['gb'] = 19;h['ee'] = 20;h['lu'] = 21;h['mt'] = 22;h['sk'] = 23;h['pt'] = 24;
h['de'] = 25;h['br'] = 26;h['ch'] = 27;

const PersonalDetailPage = Object.create(Page, {
  date: {
    get: function() {
      return browser.element(expirationDropDown);
    }
  },

  month: {
    get: function() {
      return browser.element(monthDropDown);
    }
  },

  year: {
    get: function() {
      return browser.element(yearDropDown);
    }
  },

  saveAndContinue: {
    get: function() {
      return browser.element(saveAndContinueButton);
    }
  },

  firstNameFeild: {
    get: function() {
      return browser.element(firstNameText);
    }
  },

  lastNameFeild: {
    get: function() {
      return browser.element(lastNameText);
    }
  },

  nationalId: {
    get: function() {
      return browser.element(nationalIdText);
    }
  },

  nationalIdSE: {
    get: function() {
      return browser.element(nationalIdSEText);
    }
  },

  addressline1: {
    get: function() {
      return browser.element(address);
    }
  },

  cityFeild: {
    get: function() {
      return browser.element(cityText);
    }
  },

  postalCode: {
    get: function() {
      return browser.element(postalCodeText);
    }
  },

  changeAddress: {
    get: function() {
      return browser.element(changeAddressLink);
    }
  },

  houseNumber: {
    get: function() {
      return browser.element(houseNumberText);
    }
  },

  region: {
    get: function() {
      return browser.element(regionTxt);
    }
  },

  fillAddressDetails: {
    value: function(country, testData) {
      browser.waitForVisible(address);
      PersonalDetailPage.addressline1.setValue(
        userData['userDetails']['address']
      );
      if(country=='nl'){
      PersonalDetailPage.houseNumber.setValue(
        userData['userDetails']['houseNo']
      );}
      PersonalDetailPage.postalCode.setValue(testData[country]['postalCode']);
      PersonalDetailPage.cityFeild.setValue(userData['userDetails']['city']);
      if (country == 'it') {
        browser.waitForVisible(regionTxt);
        browser.pause(2000);
        PersonalDetailPage.region.selectByValue('number:336');
      }
    }
  },

  clickSaveAndContinue: {
    value: function(country, testData) {
      browser.pause(2000);
      browser.waitForVisible(saveAndContinueButton);
      PersonalDetailPage.saveAndContinue.click();
    }
  },

  fillCEP: {
    value: function() {
      browser.waitForVisible(nationalIdText);
      PersonalDetailPage.nationalId.setValue(cpfData["cpf"]);
    }
  },

  fillPersonalDetails: {
    value: function(country, testData) {
      browser.pause(3000);
      browser.waitForVisible(firstNameText);
      PersonalDetailPage.firstNameFeild.setValue(
        userData['userDetails']['firstName']
      );
      PersonalDetailPage.lastNameFeild.setValue(
        userData['userDetails']['lastName']
      );
      console.log('country: ' + country);
      switch (country) {
        case 'it':
        case 'us':
        case 'es':
        case 'pl':
        case 'cl':
        case 'dk':
          browser.pause(3000);
          PersonalDetailPage.nationalId.setValue(
            testData[country]['nationalId']
          );
          break;
        case 'se':
          PersonalDetailPage.nationalIdSE.setValue(
            testData[country]['nationalId']
          );
          break;
      }

      for (var k in h) {
          // use hasOwnProperty to filter out keys from the Object.prototype
          if (h.hasOwnProperty(k)) {
              if(k==country){
              browser.waitForVisible(expirationDropDown);
              PersonalDetailPage.date.selectByIndex(1);
              PersonalDetailPage.month.selectByIndex(1);
              PersonalDetailPage.year.selectByIndex(20);
              break;
            }
          }
      }
    }
  }
});

module.exports = PersonalDetailPage;
