/*global require, describe, it, browser,before, after, beforeEach, exist*/

var Page = require('../../../pageobjects/page');
var expect = require('chai').expect;
var CreateAccountPage = require('../../../pageobjects/createAccount.page');
var fs = require("fs");
var contents = fs.readFileSync("test/specs/data/create-account.json");


describe('When user goes to create Sumup account page', function () {

    'use strict';

    var localePath='en-fi';
    var locale=localePath.split('-')[1];
    console.log('locale:'+locale);
    var jsonContent = JSON.parse(contents);
    var browser1=browser.desiredCapabilities.browserName;
    var invalidError='.validation-list';
    var newTab='div.page.main-content.wrapper';

    describe('And create account', function () {
        beforeEach(function() {
          browser.url('/'+'signup/create-account');
        });

        it('with invalid email id', function () {
            CreateAccountPage.fillEmailAndPassword(jsonContent["invalidEmail"]["email"],jsonContent["invalidEmail"]["password"]);
            browser.waitForVisible(invalidError);
            expect(browser.isVisible(invalidError)[0]).to.equal(true);
        });

        it('with invalid password', function () {
            CreateAccountPage.fillEmailAndPassword(jsonContent["invalidPassword"]["email"],jsonContent["invalidPassword"]["password"]);
            browser.waitForVisible(invalidError);
            expect(browser.isVisible(invalidError)[1]).to.equal(true);
        });

        it('with empty email id', function () {
            CreateAccountPage.fillEmailAndPassword(jsonContent["emptyEmail"]["email"],jsonContent["emptyEmail"]["password"]);
            browser.waitForVisible(invalidError);
            expect(browser.isVisible(invalidError)[0]).to.equal(true);
        });

        it('with empty password', function () {
            CreateAccountPage.fillEmailAndPassword(jsonContent["emptyPassword"]["email"],jsonContent["emptyPassword"]["password"]);
            browser.waitForVisible(invalidError);
            expect(browser.isVisible(invalidError)[0]).to.equal(true);
        });

        it('with already existing email id', function () {
            CreateAccountPage.fillEmailAndPassword(jsonContent["emailAlreadyRegistered"]["email"],jsonContent["emailAlreadyRegistered"]["password"]);
            CreateAccountPage.continue.click();
            browser.waitForVisible(invalidError);
            expect(browser.isVisible(invalidError)[2]).to.equal(true);
        });

        if(browser1=='chrome'){
        it('should able to open Terms and condition page', function () {
            browser.pause(1000);
            CreateAccountPage.termsAndCondition.click();
            var tabs = browser.getTabIds();
            browser.switchTab(tabs[1]);
            expect(browser.isVisible(newTab)).to.equal(true);
            browser.window();
            browser.switchTab(tabs[0]);
        });
        it('should able to open open policy page', function () {
            browser.pause(1000);
            CreateAccountPage.policy.click();
            var tabs = browser.getTabIds();
            browser.switchTab(tabs[1]);
            browser.waitForVisible(newTab);
            expect(browser.isVisible(newTab)).to.equal(true);
            browser.window();
            browser.switchTab(tabs[0]);
        });}
        it('should able to login from create account pa', function () {
            let timestamp = new Date().getTime();
            var account='test.account+'+timestamp+'@example.com';
            CreateAccountPage.fillEmailAndPassword(account,account);
            CreateAccountPage.continue.click();
            //
        });
      //  it('should able to login from create account page', function () {

      //  });
        /*it('should able to login from create account page', function () {
            browser.pause(2000);
              browser.element('/a[@href="/login"]').click();
            //  login.click();
              browser.pause(4000);
        });*/
  });

});
