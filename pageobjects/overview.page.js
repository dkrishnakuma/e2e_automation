const Page = require('./page');

const shopLink =
  'body > div.main.ng-scope > ui-view > layout > div.layout > sidenav > div > ul > li:nth-child(3) > a > span.sidenav__item-text.ng-binding.sidenav__item-text--expanded';
const newShopLink = '[ng-click="$ctrl.goToShop()"]';
const actiaveteButton = "//a[contains(@href,'activate')]";

const OverviewPage = Object.create(Page, {
  shop: {
    get: function() {
      return browser.element(newShopLink);
    }
  },

  activate: {
    get: function() {
      return browser.element(actiaveteButton);
    }
  },

  clickActivate: {
    value: function() {
      browser.waitForVisible(actiaveteButton);
      OverviewPage.activate.click();
    }
  },

  goToShop: {
    value: function(locale) {
      browser.url('/' + `${locale}` + '/shop');
    }
  },

  selectShopFromOverviewPage: {
    value: function() {
      browser.waitForVisible(newShopLink);
      OverviewPage.shop.click();
    }
  }
});

module.exports = OverviewPage;
