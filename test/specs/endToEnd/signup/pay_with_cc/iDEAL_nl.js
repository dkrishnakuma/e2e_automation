//import iDEALPage from 'pageobjects/iDEAL.page';
var Page = require('../../../../../pageobjects/page');
var expect = require('chai').expect;
var CreateAccountPage = require('../../../../../pageobjects/create_account.page');
var ShippingDetailsPage = require('../../../../../pageobjects/shipping_details.page');
var BusinessDetailPage = require('../../../../../pageobjects/business_details.page');
var PersonalDetailPage = require('../../../../../pageobjects/personal_details.page');
var BankDetailPage = require('../../../../../pageobjects/bank_details.page');
var iDEALPage = require('../../../../../pageobjects/iDEAL.page');
var DBValidation = require('../../../../../pageobjects/db_validation');
var AccountActivationPage = require('../../../../../pageobjects/account_activation.page');
var DBconfig = require('../../../dbconfig/dbconfig_staging');
var ReadFile = require('fs');
var contents = ReadFile.readFileSync('test/specs/data/test_account.json');

var locale = 'en-nl';
var testData = JSON.parse(contents);
var country = locale.split('-')[1];
let account;

('use strict');
describe('Account creation and activation', function() {
  it('should be able to go to create account page for '+country, function() {
    browser.url('/' + `${locale}` + '/signup/create-account');
    browser.windowHandleSize({ width: 1300, height: 768 });
  });
  it('should be able to enter username and password and create account', function() {
    account = CreateAccountPage.getAccount(country);
    CreateAccountPage.fillEmailAndPassword(account, account);
    browser.pause(2000);
    CreateAccountPage.privacyPolicy.click();
    CreateAccountPage.continue.click();
  });
  it('should be able to fill shipping details and select iDEAL as payment method', function() {
    ShippingDetailsPage.fillShippingDetails(country, testData);
    ShippingDetailsPage.selectiDEAL();
    ShippingDetailsPage.clickConfirmOrder();
    iDEALPage.cancel();
    ShippingDetailsPage.selectAndFillCreditCardDetails();
    ShippingDetailsPage.clickConfirmOrder();
  });
  it('should be able to fill business details and select soletraders', function() {
    ShippingDetailsPage.completeAccount();
    BusinessDetailPage.addBusinessDetails();
    BusinessDetailPage.selectSoleTraders();
    BusinessDetailPage.fillBusinessDetailsForSoleTraders(country, testData);
    BusinessDetailPage.clikSaveAndContinue();
  });
  it('should be able to fill personal details', function() {
    PersonalDetailPage.fillPersonalDetails(country, testData);
    PersonalDetailPage.clickSaveAndContinue();
    BankDetailPage.fillBankDetails(country, testData);
  });
  it('should be able to activate account', async function() {
    AccountActivationPage.enterPhoneNumber(testData[country]["activatePhoneNumber"]);
    await DBValidation.inSOAP(country, account);
  });
});
