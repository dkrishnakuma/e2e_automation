const Page = require('./page');

/*const mobileText = '#phoneNumber';
const activationButton =
  '.css-yvbbfo-ChildrenWrapper-childrenWrapperStyles';*/

//const mobileText = '#mobile';
const mobileText = '#phoneNumber';
/*const activationButton =
  '[ng-click="smsCodeRequest.submit(SmsCodeRequestForm, smsCodeRequest.mobilePhone)"]';*/
const activationButton ='.css-yvbbfo-ChildrenWrapper-childrenWrapperStyles.e1apts1r1';
const codeList = '.active-validation-list';
/*const codeButton =
  '[ng-click="smsCodeVerification.submit(smsCodeVerification.code, SmsCodeVerificationForm)"]';*/
const codeButton ='.css-dbk2de-Wrapper.e1apts1r0';
//const codeText = '#code';
const codeText = '#requestCode';

const AccountActivationPage = Object.create(Page, {
  mobile: {
    get: function() {
      return browser.element(mobileText);
    }
  },

  activateBt: {
    get: function() {
      return browser.element(activationButton);
    }
  },

  code: {
    get: function() {
      return browser.element(codeList);
    }
  },

  codeBt: {
    get: function() {
      return browser.element(codeButton);
    }
  },

  enterVerifyCode: {
    value: function(code) {
      browser.setValue(codeText, code);
    }
  },

  clickConfirmCode: {
    value: function() {
      AccountActivationPage.codeBt.click();
    }
  },

  parseString: {
    value: function(code) {
      var beforeParseCode = JSON.stringify(code);
      var parsedCode = beforeParseCode.split('"')[3];
      return parsedCode;
    }
  },

  parseNumeric: {
    value: function(code) {
      var beforeParseCode = JSON.stringify(code);
      var parsedCode = beforeParseCode.split(':')[1];
      var removedBrackets = parsedCode.slice(0, -2);
      return removedBrackets;
    }
  },

  enterPhoneNumber: {
    value: function(phoneNumber) {
      browser.waitForVisible(mobileText);
      AccountActivationPage.mobile.setValue(phoneNumber);
      browser.pause(2000);
      browser.waitForVisible(activationButton);
      AccountActivationPage.activateBt.click();
    }
  }
});
module.exports = AccountActivationPage;
