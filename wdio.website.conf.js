// Let's load the default configs:
var defaults = require("./wdio.conf.js").config;
var _ = require("lodash");

var overrides = {
    baseUrl: "https://sam-app.ro/"
};

// Send the merged config to wdio
exports.config = _.defaultsDeep(overrides, defaults);
