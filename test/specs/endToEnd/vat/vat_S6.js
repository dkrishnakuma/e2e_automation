/*global require, describe, it, browser,before, after, beforeEach, exist*/

var Page = require('../../../../pageobjects/page');
var expect = require('chai').expect;
var CreateAccountPage = require('../../../../pageobjects/create_account.page');
var ShippingDetailsPage = require('../../../../pageobjects/shipping_details.page');
var BusinessDetailPage = require('../../../../pageobjects/business_details.page');
var PersonalDetailPage = require('../../../../pageobjects/personal_details.page');
var BankDetailPage = require('../../../../pageobjects/bank_details.page');
var AccountActivationPage = require('../../../../pageobjects/account_activation.page');
var DBValidation = require('../../../../pageobjects/db_validation');
var DBconfig = require('../../dbconfig/dbconfig_beta');
var ReadFile = require('fs');
var contents = ReadFile.readFileSync('test/specs/data/invalid_vat.json');

//var locale = 'en-fr';
var localeList = [{"locale" : "en-at"},{"locale" : "en-be"}];
/*var localeList = [{"locale" : "en-at"},{"locale" : "en-be"},{"locale" : "en-bg"},{"locale" : "en-cy"},{"locale" : "en-cz"}
,{"locale" : "en-fi"},{"locale" : "en-fr"},{"locale" : "en-hu"},{"locale" : "en-ie"},{"locale" : "en-it"},{"locale" : "en-lv"}
,{"locale" : "en-lt"},{"locale" : "en-nl"},{"locale" : "en-pl"},{"locale" : "en-pt"},{"locale" : "en-si"},{"locale" : "en-es"}
,{"locale" : "en-se"},{"locale" : "en-gb"},{"locale" : "en-dk"},{"locale" : "en-ee"},{"locale" : "en-lu"},{"locale" : "en-mt"}
,{"locale" : "en-sk"}];*/

var shippingData = JSON.parse(contents);

var account;

var shipList = [{"ship" : "at"},{"ship" : "be"}];

var vatList = [{"vat" : "fr"}];

('use strict');
describe('Account creation and activation', async function() {

  function testScenario(shipCountry,vatCountry,localeObject) {
    it('should be able to go to create account page and create account for '+localeObject.locale, async function() {
      var country = localeObject.locale.split('-')[1];
       browser.url('/' + `${localeObject.locale}` + '/signup/create-account');
       browser.windowHandleSize({ width: 1300, height: 768 });
       account =  CreateAccountPage.getAccount(country);
       CreateAccountPage.fillEmailAndPassword(account, account);
       CreateAccountPage.continue.click();
       ShippingDetailsPage.fillShippingDetailsForVAT(shipCountry.ship,shippingData,vatCountry.vat);
       ShippingDetailsPage.selectAndFillCreditCardDetails();
       ShippingDetailsPage.clickConfirmOrder();
       await DBValidation.vatValidation(country, account);
    });
  }

  for (var i = 0; i < shipList.length; i++) {
    var localeObject = localeList[i];
    console.log('localeObject' + localeObject.locale);
    var shipCountry = shipList[i];
    var vatCountry = vatList[0];
    console.log('shipCountry' + shipCountry.ship)
    testScenario(shipCountry,vatCountry,localeObject);
  }
});
