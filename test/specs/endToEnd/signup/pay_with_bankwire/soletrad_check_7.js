/*global require, describe, it, browser,before, after, beforeEach, exist*/

var Page = require('../../../../../pageobjects/page');
var expect = require('chai').expect;
var CreateAccountPage = require('../../../../../pageobjects/create_account.page');
var ShippingDetailsPage = require('../../../../../pageobjects/shipping_details.page');
var BusinessDetailPage = require('../../../../../pageobjects/business_details.page');
var PersonalDetailPage = require('../../../../../pageobjects/personal_details.page');
var BankDetailPage = require('../../../../../pageobjects/bank_details.page');
var AccountActivationPage = require('../../../../../pageobjects/account_activation.page');
var DBValidation = require('../../../../../pageobjects/db_validation');
var DBconfig = require('../../../dbconfig/dbconfig_staging');
var ReadFile = require('fs');
var contents = ReadFile.readFileSync('test/specs/data/test_account.json');

//var localeList = [{"locale" : "sv-se"}];
var localeList = [{"locale" : "sv-se"},{"locale" : "de-ch"},{"locale" : "en-gb"},{"locale" : "fr-fr"}];
var testData = JSON.parse(contents);

('use strict');
describe('Account creation and activation', function() {

  function createAccount(localeObject) {
  it('and user should able to order a terminal using bankwire as sole traders for '+localeObject.locale, async function() {
    var country = localeObject.locale.split('-')[1];
    console.log('country' + country);
      browser.url('/' + `${localeObject.locale}` + '/signup/shop?guest');
      browser.windowHandleSize({ width: 1300, height: 768 });
      var account = CreateAccountPage.getAccount(country);
      console.log('account' + account);
      browser.pause(2000);
      browser.setValue('#email',account);

      ShippingDetailsPage.fillShippingDetails(country, testData);
      //ShippingDetailsPage.selectAndFillBankWire();
      ShippingDetailsPage.selectAndFillCreditCardDetails();
      browser.pause(2000);
      browser.click('.css-5ctdxe-CheckboxLabel-checkbox__label-labelBaseStyles.e1ajo7p61');
      ShippingDetailsPage.clickConfirmOrder();
      browser.pause(4000);
      browser.setValue('#password',account);
      browser.pause(2000);
      CreateAccountPage.privacyPolicy.click();
      browser.pause(2000);
      browser.click('.e1shq2kf4');
      BusinessDetailPage.addBusinessDetails();
      BusinessDetailPage.selectSoleTraders();
      BusinessDetailPage.fillBusinessDetailsForSoleTraders(country, testData);
      BusinessDetailPage.clikSaveAndContinue();
      /*PersonalDetailPage.fillPesrsonalDetails(country, testData);
      PersonalDetailPage.clickSaveAndContinue();
      BankDetailPage.fillBankDetails(country, testData);
      AccountActivationPage.enterPhoneNumber(testData[country]["activatePhoneNumber"]);
      await DBValidation.inSOAP(country, account);*/
  });}

  for (var i = 0; i < localeList.length; i++) {
    var localeObject = localeList[i];
    console.log('localeObject' + localeObject);
    createAccount(localeObject);
  }
});
