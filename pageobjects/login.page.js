const Page = require('./page');

const usernameText = '#username';
const passwordText = '#password';
const loginBt = '.pre-auth__login-btn';
const resetPasswordText = '[ui-sref="i18n.resetPassword"]';
const selectItalyFromDropdownMenu =
  'body > dialogue > div.dialogue > div > ul > li:nth-child(1) > country-selector > ul > li.ng-scope._990a41827ba93e14d1f7447253d27c1c > a > svg > use';
const forgotPasswordLink = '.pre-auth__footer-left a.ng-binding';
const createAccountLink = '.pre-auth__footer-right a.ng-binding';
const continueButton = '/html/body/dialogue/div[1]/div/ul/li[2]/div/a[1]';
const ccontinueOnCreateAccountButton =
  '[ng-click="dialogue.handleCountry(dialogue.country)"]';

const LoginPage = Object.create(Page, {
  email: {
    get: function() {
      return browser.element(usernameText);
    }
  },
  password: {
    get: function() {
      return browser.element(passwordText);
    }
  },
  loginButton: {
    get: function() {
      return browser.element(loginBt);
    }
  },

  selectItalyFromDropdown: {
    get: function() {
      return browser.element(selectItalyFromDropdownMenu);
    }
  },

  forgotPassword: {
    get: function() {
      return browser.element(forgotPasswordLink);
    }
  },

  createAccount: {
    get: function() {
      return browser.element(createAccountLink);
    }
  },

  continue: {
    get: function() {
      return browser.element(continueButton);
    }
  },

  resetPassword: {
    get: function() {
      return browser.element(resetPasswordText);
    }
  },

  continueOnCreateAccount: {
    get: function() {
      return browser.element(ccontinueOnCreateAccountButton);
    }
  },

  /**
   * define or overwrite page methods
   */

  performLogin: {
    value: function(email, password) {
      browser.waitForVisible(usernameText);
      LoginPage.email.setValue(email);
      LoginPage.password.setValue(password);
      LoginPage.clickLogin();
    }
  },

  clickLogin: {
    value: function() {
      LoginPage.loginButton.click();
    }
  },

  selectItalyAsCountry: {
    value: function() {
      browser.waitForVisible(selectItalyFromDropdownMenu);
      LoginPage.selectItalyFromDropdown.click();
    }
  },

  clickResetPassword: {
    value: function() {
      browser.waitForVisible(resetPasswordText);
      LoginPage.resetPassword.click();
      browser.pause(2000);
    }
  },

  clickCreateAccount: {
    value: function() {
      LoginPage.createAccount.click();
      LoginPage.continueOnCreateAccount.click();
      browser.pause(2000);
    }
  }
});

module.exports = LoginPage;
