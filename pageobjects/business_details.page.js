const Page = require('./page');
const ReadFile = require('fs');

const contents = ReadFile.readFileSync('test/specs/data/user_details.json');
const userData = JSON.parse(contents);
const cnpjValue = ReadFile.readFileSync('test/specs/data/cnpj.json');
const cnpjData = JSON.parse(cnpjValue);
const saveAndContinueButton = 'button.btn.btn--highlight.ng-binding';
const addBusinessButton = '/html/body/dialogue/div[1]/div/div[2]/ul/li/a';
const soleTraderOption = 'label[for="soleTraderTrue"]';
const comapnyOption = 'label[for="soleTraderFalse"]';
const companyNameText = '#companyName';
const businessCategoryDropDown = 'select[name="merchantCategoryCode"]';
const amountOfTransactionDropDown = 'select[name="expectedTransactionAmount"]';
const phoneNumberText = 'input[name="landline"]';
const legalTypeDropDown = 'select[name="legalType"]';
const address1Text = '#address-line1';
const houseNumberText = '#house_number';
const postalCodeText = '#post-code';
const cityText = '#city';
const regionText = '#region';
const cep = '#post_code_lookup';
const ownersFirstNameText = '[name="firstName"]';
const ownersLastNameText = '[name="lastName"]';
const changeAddressLink = 'a[href="#"].ng-binding';
const nationalIdText = 'input[name="companyRegistrationNumber"]';
const businessNameText = '#doingBusinessAs';
const comapnyRegistrationNumberText = '[name="companyRegistrationNumber"]';
const natureAndPurposeText = '[name="natureAndPurpose"]';

const BusinessDetailPage = Object.create(Page, {
  addBusinessDetailsBt: {
      get: function () {
          return browser.element(addBusinessButton);
      }
  },

  soleTrader: {
      get: function () {
          return browser.element(soleTraderOption);
      }
  },

  natureAndPurpose: {
      get: function () {
          return browser.element(natureAndPurposeText);
      }
  },

  company: {
      get: function () {
          return browser.element(comapnyOption);
      }
  },

  companyRegistrationNumber: {
      get: function () {
          return browser.element(comapnyRegistrationNumberText);
      }
  },

  companyName: {
      get: function () {
          return browser.element(companyNameText);
      }
  },

  businessNameFeild: {
      get: function () {
          return browser.element(businessNameText);
      }
  },

  businessCategory: {
      get: function () {
          return browser.element(businessCategoryDropDown);
      }
  },

  amountOfTransaction: {
      get: function () {
          return browser.element(amountOfTransactionDropDown);
      }
  },

  phoneNumber: {
      get: function () {
          return browser.element(phoneNumberText);
      }
  },

  nationalId: {
      get: function () {
          return browser.element(nationalIdText);
      }
  },

  saveAndContinue: {
      get: function () {
          return browser.element(saveAndContinueButton);
      }
  },

  legalType: {
      get: function () {
          return browser.element(legalTypeDropDown);
      }
  },

  address1: {
      get: function () {
          return browser.element(address1Text);
      }
  },

  houseNumber: {
      get: function () {
          return browser.element(houseNumberText);
      }
  },

  postalCode: {
      get: function () {
          return browser.element(postalCodeText);
      }
  },

  postalCodeForBR: {
      get: function () {
          return browser.element(cep);
      }
  },

  cityFeild: {
      get: function () {
          return browser.element(cityText);
      }
  },

  ownersFirstName: {
      get: function () {
          return browser.element(ownersFirstNameText);
      }
  },

  ownersLastName: {
      get: function () {
          return browser.element(ownersLastNameText);
      }
  },

  changeAddress: {
      get: function () {
          return browser.element(changeAddressLink);
      }
  },

  addressline1: {
      get: function () {
          return browser.element(address1Text);
      }
  },

  region: {
      get: function () {
          return browser.element(regionText);
      }
  },

  fillAddressDetails: {
      value: function (country,testData) {
          browser.waitForVisible(address1Text);
          BusinessDetailPage.addressline1.setValue(userData["userDetails"]["address"]);
          if(country=='nl'){
          BusinessDetailPage.houseNumber.setValue(userData["userDetails"]["houseNo"]);}
          BusinessDetailPage.postalCode.setValue(testData[country]["postalCode"]);
          BusinessDetailPage.cityFeild.setValue(userData["userDetails"]["city"]);
          switch(country){
            case 'it':
            case 'pl':
            case 'es':
            browser.waitForVisible(regionText);
            browser.pause(2000);
          BusinessDetailPage.region.selectByIndex(4);
          }
      }
  },

  fillBusinessDetailsForSoleTraders: {
      value: function (country,testData,phoneNumber) {
          if(country!='se' && country!='br' && country!='ch' && country!='cl' &&
          country!='ie' && country!='es' && country!='us' && country!='be'
         && country!='cy' && country!='cz' && country!='fi' && country!='lv' && country!='lt'
         && country!='no' && country!='si' && country!='gb' && country!='dk' && country!='ee' && country!='lu'
         && country!='mt' && country!='sk' && country!='pt' && country!='fr'){
            BusinessDetailPage.legalType.selectByIndex(2);
          }
          BusinessDetailPage.companyName.setValue(userData["userDetails"]["compName"]);
          if(country=='gb'){
            BusinessDetailPage.businessCategory.selectByIndex(2);
          } else {
            BusinessDetailPage.businessCategory.selectByIndex(1);
          }
          if(country!='br'){
          BusinessDetailPage.amountOfTransaction.selectByIndex(1);}
          if(country=='cl'){
            BusinessDetailPage.businessNameFeild.setValue(userData["userDetails"]["businessName"]);}
          /*if(country=='gb'){
            BusinessDetailPage.natureAndPurpose.setValue(userData["userDetails"]["natureAndPurpose"]);}*/
          if(country=='bg'){
            BusinessDetailPage.companyRegistrationNumber.setValue(testData[country]["companyRegistrationNumber"]);}
          BusinessDetailPage.phoneNumber.setValue(testData[country]["phoneNumber"]);
        //BusinessDetailPage.phoneNumber.setValue(phoneNumber);
         }

  },

  fillBusinessDetailsForCompany: {
      value: function (country,testData) {
          BusinessDetailPage.legalType.selectByIndex(2);
          BusinessDetailPage.companyName.setValue(userData["userDetails"]["compName"]);
          BusinessDetailPage.businessCategory.selectByIndex(1);
          if(country!='br'){
          BusinessDetailPage.amountOfTransaction.selectByIndex(1);}
          BusinessDetailPage.phoneNumber.setValue(testData[country]["phoneNumber"]);
          BusinessDetailPage.ownersFirstName.setValue(userData["userDetails"]["firstName"]);
          BusinessDetailPage.ownersLastName.setValue(userData["userDetails"]["lastName"]);
      }
  },

  fillCNPJ: {
      value: function () {
      browser.waitForVisible(nationalIdText);
      BusinessDetailPage.nationalId.setValue(cnpjData["cnpj"]);
      }
  },

  fillPostalCodeAndPhoneNumber: {
      value: function (country,testData) {
          BusinessDetailPage.phoneNumber.setValue(testData[country]["phoneNumber"]);
          BusinessDetailPage.postalCodeForBR.setValue(testData[country]["postalCode"]);
          browser.waitForVisible(houseNumberText);
          BusinessDetailPage.houseNumber.setValue(userData["userDetails"]["houseNo"]);
      }
  },

  addBusinessOwner: {
       value: function () {
         BusinessDetailPage.ownersFirstName.setValue(userData["userDetails"]["firstName"]);
         BusinessDetailPage.ownersLastName.setValue(userData["userDetails"]["lastName"]);
       }
   },

 addBusinessDetails: {
      value: function (country,testData) {
          browser.waitForVisible(addBusinessButton);
          BusinessDetailPage.addBusinessDetailsBt.click();
      }
  },

  selectSoleTraders: {
       value: function (country,testData) {
           browser.waitForVisible(soleTraderOption);
           BusinessDetailPage.soleTrader.click();
       }
   },

  selectCompany: {
      value: function (country,testData) {
          BusinessDetailPage.company.click();
      }
  },

  clikSaveAndContinue: {
      value: function () {
        browser.pause(2000);
        browser.waitForVisible(saveAndContinueButton);
        BusinessDetailPage.saveAndContinue.click();
        browser.pause(2000);
      }
  }
});

module.exports = BusinessDetailPage;
